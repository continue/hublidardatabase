# -*- coding: utf-8 -*-
"""
@author: espa

Based on specific lidar configurations the users desire, this code will extract
the data from the NETcdf file, to provide the user with the measurements
desire based on the lidar configuration and the sampling frequency per beam. 

Noticed that in this example, the measurements are performed consecuently, and
without swithching delay. 

On this script, you will get:
- Dictionary DF_lidar for hub-lidar measurements for the wind turbine.
- df_aero: Aeroelastic response for the wind turbine.
- u_turb, v_turb, w_turb, u_shear_turb: Mann turbulence box values for the wind turbine.
  This can be generate four .bin files for the turbulence boxes, as HAWC2 read them, if the
  option hawc2_file is True.

"""
import pickle
import numpy as np
import pandas as pd
from _ltb_values import kw_turbgen, kw_dtu10mw
import hulidb.data_extraction as dext
import hulidb.functions_NETcdf as fNet
import hulidb.functions as func

# %% Defining the lidar configurations to be evaluated:

# Cases to be evaluated based on the dict:
#Cases = fNet.generate_Cases(**kw_turbgen)

# Cases to be evaluated based on the available inflow cases in the ./NetCDF/ folder:
folder_path = './NETcdf/'
Cases = func.check_files_and_convention(folder_path)

# Loading lidar configurations for all available cases.
# This file was generated from script 03-NETcdf_generation.py
# The file is already available in the repo. If not, please genreate the file.
lidar_csv = ('.\\files\\Lidar_beams_hawc2.csv')

# Read each beam in HAWC2 output available:
df_lidar = pd.read_csv(lidar_csv)
df_lidar = df_lidar.drop(['Unnamed: 0', 'Beggining', 'Tag'], axis=1)

    
# Definition of selected configurations (This is an example of three configs.):
config_1 = {'theta': [0, 20, 30, 30, 30],  
            'psi': [0, 60, 120, 180, 240],
            'Focus-Length': np.concatenate((np.arange(50, 200, 10), 
                                      np.arange(200, 505, 25))),
            }

config_2 = {'theta': [0, 20, 30, ],  
            'psi': [0, 75, 125],
            'Focus-Length': np.concatenate((np.arange(50, 200, 10), 
                                      np.arange(200, 505, 25))),
            }

config_3 = {'theta': [0],  
            'psi': [0],
            'Focus-Length': np.arange(50, 310, 10),
            }

lidar_config = [config_1, config_2, config_3]


# Define parameters for the lidar extraction data: 
sampling_freq = 5  # [Hz] per beam
t_start, t_end = 100.05, 700.05  # This time is also constrained as the available time from HAWC2 simulation
deltat = 0.05   # delta time from HAWC2 simulation output channels.

# The dictionary with the lidar configurations, can be saved in a pickle file for future use.
path_to_save = './files/'    # Path to save the pickle file
file_name = 'df_huli'   # Name of the pickle file to be saved.

# Extract Hub-lidar data and generates a dictionary with the extracted data:
DF_lidar = dext.extract_huli_data(Cases, df_lidar, lidar_config, 
                                sampling_freq, t_start, t_end, deltat,
                                path=path_to_save, fname=file_name, save=True)

# Test load pickle file saved to compared:    
#DF_lidar_loaded = dext.load_dictionary(path_to_save, file_name)

# Extract Aeroelastic response: 
path = './NETcdf/'  # Path where netcdf files are saved.
fname = 'df_aero'  # Name of the file to be saved for aeroelastic response.

# Extract aeroelastic response from the NETcdf file:
df_aero = dext.extract_aero_resp(Cases, path, path_to_save=path_to_save, fname=fname, save=True)

# Test load pickle file to compared: 
#DF_aero_loaded = dext.load_dictionary(path_to_save, fname)

# Extract Mann turbulence box values:
path_netcdf = './NETcdf/'
path_turb_save = './dtu_10mw/turb_saved/'  # Path to save the turbulence box values extracted.

# Extract turbulence box values from the NETcdf file:
# If hawc2_file is True, it will generate four .bin files for the turbulence boxes, as HAWC2 read them.
# The four files are: u, v, w components, plus u_shear, which is the u component plus added shear profile.
for case in Cases: 
    dext.extract_tbox_from_netcdf(case, path_netcdf, path_turb_save, hawc2_file=True)

