"""
@author: espa

Dictionaries with variables and parameters for:
    - DTU 10MW Wind turbine model for HAWC2.
    - Turbulence box generation information.
    - Lidar Configuration generation.
    - Testing cases for the above dictionaries.

If any of the parameters of the simulation want to be modified, you need
to select the proper dictionary. If you want to test multiple option, for
example, multiple wind turbine models, I would recommend to create aditional
dictionaries, following the same structure as below, and then calling them from
the script to be used in the function.

For more information, please refer to documentation:
https://gitlab.windenergy.dtu.dk/espa/lidardatabase/-/blob/main/README.md  # FIXME
"""

# %%

import os
import numpy as np

# To locate Mann generator executable:
CURRENT_DIR = os.path.dirname(__file__)
PROJECT_ROOT = os.path.abspath(os.path.join(CURRENT_DIR, os.pardir))


# Dictionary for DTU10MW wind turbine model for HAWC2
kw_dtu10mw = {'tilt': 5*np.pi/180,  # tilt [rad]
              'shaft_length': -7.1,  # shaft length [m]
              'z_hub': -119,  # hub height [m]
              'D': 178,  # rotor diameter [m]
              # 'Ly': 400,  # width of turbulence box [m]
              # 'Lz': 400,  # height of turbulence box [m]
              'name_wt_model': 'dtu_10mw', # name of wind turbine model
              'dt_output': 0.05,  # Time step HAWC2 output results
              'init_idx_lidar': 120,  # HAWC2 Channel for first HuLi Lidar
              'path_res': './dtu_10mw/res/',  # path for HAWC2 results
              }


# Dictionary for Mann-turbulence box generation in HAWC2
kw_turbgen = {'Nx': 8192,  # Number of grid points in the x direction
              'Ny': 128,  # Number of grid points in the y direction
              'Nz': 128,  # Number of grid points in the z direction
              'hub_pos_x0': 0,  # Initial x position in HAWC2 glb. coord. system
              'hub_pos_y0': 0,  # Initial y position in HAWC2 glb. coord. system
              'hub_pos_z0': -119,  # Initial z coordinate System HAWC2
              'box_center': -200,
              'Time': 800,  # Total time for turbulence box generation
              'dt_simu': 0.01,  # Delta t for HAWC2 Simulation
              'start_time': 100, # Start time for getting data in HAWC2
              'alpha': 0.2,  # Shear exponent for shear profile in HAWC2
              'Ly': 400,  # Length turb_box in the lateral Y direction
              'Lz': 400,   # Lenght turb_box in the vertical Z direction
              'tb_dy': 400 / (128-1),  #  Deltay for each grid in Y
              'tb_dz': 400 / (128-1),  # Deltaz for each grid in Z
              'L': 33.6,  # Mann Model length scale parameter
              'gamma': 3.9,  # Mann Model Anasotropy parameter
              'HighFreqComp': 1,  # If 1, HighFreqComp is True, if 0, False.
              'wsps': [8.0, 11.4, 18.0],  # Wind Speed [m/s]
              'Seeds': [572, 927, 1001],  # Seed Number
              'I_ref': 0.14,  # Desire Turbulence intensity
              'turb_model': 'dtu_10mw',  # Wind turbine model
              # Path to save Turbulence Boxes binary files:
              'path_save': './dtu_10mw/turb/'
              }
    

# Dictionary for Mann-turbulence box generation in HAWC2
kw_turbgen_test = {'Nx': 8192,  # Number of grid points in the x direction
                   'Ny': 32,  # Number of grid points in the y direction
                   'Nz': 32,  # Number of grid points in the z direction
                   'hub_pos_x0': 0,  # Initial x position in HAWC2 glb. coord. system
                   'hub_pos_y0': 0,  # Initial y position in HAWC2 glb. coord. system
                   'hub_pos_z0': -119,  # Initial z coordinate System HAWC2
                   'box_center': -200,
                   'Time': 800,  # Total time for turbulence box generation
                   'dt_simu': 0.01,  # Delta t for HAWC2 Simulation
                   'start_time': 100, # Start time for getting data in HAWC2
                   'alpha': 0.2,  # Shear exponent for shear profile in HAWC2
                   'Ly': 400,  # Length turb_box in the lateral Y direction
                   'Lz': 400,   # Lenght turb_box in the vertical Z direction
                   'tb_dy': 400 / (32-1),  #  Deltay for each grid in Y
                   'tb_dz': 400 / (32-1),  # Deltaz for each grid in Z
                   'L': 33.6,  # Mann Model length scale parameter
                   'gamma': 3.9,  # Mann Model Anasotropy parameter
                   'HighFreqComp': 1,  # If 1, HighFreqComp is True, if 0, False.
                   'wsps': [13.4],  # Wind Speed [m/s]
                   'Seeds': [777],  # Seed Number
                   'I_ref': 0.14,  # Desire Turbulence intensity
                   'turb_model': 'dtu_10mw',  # Wind turbine model
                   # Path to save Turbulence Boxes binary files:
                   'path_save': './dtu_10mw/turb/'
                   }

# Lidar parameters for generation of output beams in HAWC2
lidar_arg = {
    # Half-cone angle of lidar beam:
    'theta': np.concatenate((np.array([0, 5]), np.arange(10, 30, 2.5),
                             np.arange(30, 60, 5), np.arange(60, 91, 10))),
    'psi': np.arange(0, 360, 5),  # Azimuthal angle lidar beam
    'ranges': np.concatenate((np.arange(50, 200, 10), 
                              np.arange(200, 505, 25))),  # Ranges
    'beamno_htc': 2000,  # Number of beams per htc file
    'min_height': 0,  # Minimum height in Z to filter Hub-lidar data
    'max_height': 420, # Maximum height in Z to filter Hub-lidar data
    'htc_begin': ' aero hub_lidar ',  # HAWC2 output line
    'deltaP': 38.4,  # range-gate length
    'deltaL': 24.75,  # lidar beam full width at half-Maximum
    'dv': 3,  # Half-width of the integration volume
    'N_int': 200,  # Number of integration points in dv
    'config_path': '.\\files\\',  # path where files are saved
    'config_file': 'Lidar_beams_hawc2.csv',  # File name
    }
    

# Lidar parameters for generation of output beams in HAWC2
lidar_arg_test = {
    # Half-cone angle of lidar beam:
    'theta': [0, 20, 25, 30],  # Values for testing case
    'psi': np.arange(0, 360, 30),  # Values for testing case
    'ranges': np.arange(50, 310, 10),  # Values for testing case
    'config_file': 'Lidar_beams_hawc2_test.csv',  # Test config
    'beamno_htc': 200,
    'min_height': 0,  # Minimum height in Z to filter Hub-lidar data
    'max_height': 420, # Maximum height in Z to filter Hub-lidar data
    'htc_begin': ' aero hub_lidar ',  # HAWC2 output line
    'deltaP': 38.4,  # range-gate length
    'deltaL': 24.75,  # lidar beam full width at half-Maximum
    'dv': 3,  # Half-width of the integration volume
    'N_int': 200,  # Number of integration points in dv
    'config_path': '.\\files\\',  # path where files are saved
    }
