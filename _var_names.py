'''
@author: espa

Definition of variable names and metadata for the data extracted from HAWC2
results, and stored on the NetCDF files.
'''

# Hub-lidar Metadata for the variables
HuLi_metadata = {
    'HuLi_Xg': {'units': 'meters',
           'description': 'Location in Xg-coordinate in HAWC2 global coordinate system'},
    'HuLi_Yg': {'units': 'meters',
           'description': 'Location in Yg-coordinate in HAWC2 global coordinate system'},
    'HuLi_Zg': {'units': 'meters',
           'description': 'Location in Zg-coordinate in HAWC2 global coordinate system'},
    'HuLi_V_LOS_wgh': {'units': 'm/s',
                  'description': 'Volume-averaged line-of-sight wind speed,'
                  ' at location (Xg, Yg, Zg)'},
    'HuLi_V_LOS_nom': {'units': 'm/s',
                  'description': 'Nominal line-of-sight wind speed,'
                  ' at location (Xg, Yg, Zg)'}
    }


# Turbulence Boxes metadata from HAWC2 perspective:
turb_box_metadata = {
    't': {
        'units': 'seconds',
        'description': 'Time step (equivalent to the longitudinal direction)'
        'equal to the time step of the HAWC2 simulation.'
        },
    'Xg': {
        'units': 'meters',
        'description': 'Lateral distance turbulence box in meters.'
        'This coordinate array matchs HAWC2 global coordinate system: Xg.'
        },
    'Zg': {
        'units': 'meters',
        'description': 'Vertical distance in meters, from zero (bottom) up '
        'to 400 meters (top). NOTE: It is NOT equal to HAWC2 global coordinate'
        ' system.'
        },
    'u': {'units': 'm/s', 
          'description': 'Longitudinal wind speed component at (t, Xg, Zg)'},
    'v': {'units': 'm/s', 
          'description': 'Lateral wind speed component, at (t, Xg, Zg)'},
    'w': {'units': 'm/s', 
          'description': 'Vertical wind speed component, at (t, Xg, Zg)'},
    }


# Mann Turbulence Boxes metadata, to get turbulence box: 
mann_tbox_metadata = {
    'x': {
        'units': 'meters',
        'description': 'Longitudinal distance turbulence box, from zero up '
        'to the end of the turbulence box, which is calculated as the total '
        'simulation time multiply by the average wind speed at hub height.'
        'NOTE: It is NOT equal to HAWC2 global coordinate system.'
        },
    'y': {
        'units': 'meters',
        'description': 'Lateral distance turbulence box in meters.'
        'This coordinate array matchs HAWC2 global coordinate system: Xg.'
        },
    'z': {
        'units': 'meters',
        'description': 'Vertical distance in meters, from zero (bottom) up '
        'to 400 meters (top). NOTE: It is NOT equal to HAWC2 global coordinate'
        ' system.'
        },
    't': {
        'units': 'seconds',
        'description': 'Time corresponding to HAWC2 simulation.'
        },
    'u': {'units': 'm/s', 
          'description': 'Longitudinal wind speed component at (x, y, z)'},
    'v': {'units': 'm/s', 
          'description': 'Lateral wind speed component, at (x, y, z)'},
    'w': {'units': 'm/s', 
          'description': 'Vertical wind speed component, at (x, y, z)'},
    'u_shear': {
        'units': 'm/s', 
        'description': 'Longitudinal wind speed component "u" with added '
        'shear profile, at (x, y, z)'}
    }


# Aeroelastic response metadata for the variables in the NETcdf file
aero_metadata = {
    'Time': {'units': 's', 'description': 'Time from HAWC2 simulation.'},
    'shaft_rot_angle': {
        'units': 'deg',
        'description': 'Shaft rotational angle'
        },
    'shaft_rot_angle speed': {
        'units': 'rpm',
        'description': 'Shaft rotational speed'
        },
    'pitch1_angle': {
        'units': 'deg',
        'description': 'Blade 1 pitch angle'
        },
    'pitch1_angle speed': {
        'units': 'deg/s',
        'description': 'Blade 1 pitch angle speed'
        },
    'pitch2_angle': {'units': 'deg', 'description': 'Blade 2 pitch angle'},
    'pitch2_angle speed': {
        'units': 'deg/s',
        'description': 'Blade 2 pitch angle speed'
        },
    'pitch3_angle': {'units': 'deg', 'description': 'Blade 3 pitch angle'},
    'pitch3_angle speed': {
        'units': 'deg/s',
        'description': 'Blade 3 pitch angle speed'
        },
    'Rotor_speed': {
        'units': 'rad/s',
        'description': 'Rotor speed'
        },
    'torque': {'units': 'kNm', 'description': 'Aero rotor torque'},
    'Aero_power': {'units': 'kW', 'description': 'Aero rotor power'},
    'thrust': {'units': 'kN', 'description': 'Aero rotor thrust'},
    'fw_Vx': {
        'units': 'm/s', 
        'description': 'Lateral free wind speed component Vx (v) on glb. '
        'coord. system'
        },
    'fw_Vy': {'units': 'm/s',
              'description': 'Longitudinal free wind speed component Vy (u)'
              ' on global coordinate system'},
    'fw_Vz': {'units': 'm/s',
              'description': 'Vertical free wind speed component Vz (w) '
              'on global coordinate system'},
    'WSP_abs_vhor': {'units': 'm/s', 'description': 'Longitudinal free wind '
                     'speed absolute at hub-height (0,0, -119)'},
    'WSP_Vdir_hor': {'units': 'deg', 'description': 'Direction of free wind '
                     'speed at hub-height (0,0,-119)'},
    'raws': {'units': 'm/s', 'description': 'Rotor average wind speed, Vy, '
             'including only the longitudinal wind speeds.'},
    'TB_Mx': {'units': 'kNm', 'description': 'Tower base fore-aft momement'},  
    'TB_My': {'units': 'kNm', 'description': 'Tower base side-side momement'},
    'TB_Mz': {'units': 'kNm', 'description': 'Tower base fore-aft momement'},
    'TYB_Mx': {'units': 'kNm', 'description': 'Tower yaw bearing Mx momement'},
    'TYB_My': {'units': 'kNm', 'description': 'Tower yaw bearing My momement'},
    'TYB_Mz': {'units': 'kNm', 'description': 'Tower yaw bearing Mz momement'},
    'MB_Mx': {'units': 'kNm', 'description': 'Main bearing Mx momement'},
    'MB_My': {'units': 'kNm', 'description': 'Main bearing My momement'},
    'MB_Mz': {'units': 'kNm', 'description': 'Main bearing Mz momement'},
    'BR1_Mx_ipop': {
        'units': 'kNm',
        'description': 'Blade 1 root ipop Mx momement'
        },
    'BR1_My_ipop': {
        'units': 'kNm',
        'description': 'Blade 1 root ipop My momement'
        },
    'BR1_Mz_ipop': {
        'units': 'kNm',
        'description': 'Blade 1 root ipop Mz momement'
        },
    'BR2_Mx_ipop': {
        'units': 'kNm',
        'description': 'Blade 2 root ipop Mx momement'
        },
    'BR2_My_ipop': {
        'units': 'kNm',
        'description': 'Blade 2 root ipop My momement'
        },
    'BR2_Mz_ipop': {
        'units': 'kNm',
        'description': 'Blade 2 root ipop Mz momement'
        },
    'BR3_Mx_ipop': {
        'units': 'kNm', 'description': 'Blade 3 root ipop Mx momement'
        },
    'BR3_My_ipop': {
        'units': 'kNm', 'description': 'Blade 3 root ipop My momement'
        },
    'BR3_Mz_ipop': {
        'units': 'kNm', 'description': 'Blade 3 root ipop Mz momement'
        },
    'BR1_Mx_flped': {
        'units': 'kNm', 'description': 'Blade 1 root flped Mx momement'
        },
    'BR1_My_flped': {
        'units': 'kNm', 'description': 'Blade 1 root flped My momement'
        },
    'BR1_Mz_flped': {
        'units': 'kNm', 'description': 'Blade 1 root flped Mz momement'
        },
    'BR2_Mx_flped': {
        'units': 'kNm', 'description': 'Blade 2 root flped Mx momement'
        },
    'BR2_My_flped': {
        'units': 'kNm', 'description': 'Blade 2 root flped My momement'
        },
    'BR2_Mz_flped': {
        'units': 'kNm', 'description': 'Blade 2 root flped Mz momement'
        },
    'BR3_Mx_flped': {'units': 'kNm',
                     'description': 'Blade 3 root flped Mx momement'},
    'BR3_My_flped': {'units': 'kNm',
                     'description': 'Blade 3 root flped My momement'},
    'BR3_Mz_flped': {'units': 'kNm',
                     'description': 'Blade 3 root flped Mz momement'},
    'Bld1_50_local_Mx': {'units': 'kNm',
                         'description': 'Blade 1 50% local Mx momement'},
    'Bld1_50_local_My': {'units': 'kNm',
                         'description': 'Blade 1 50% local My momement'},
    'Bld1_50_local_Mz': {'units': 'kNm',
                         'description': 'Blade 1 50% local Mz momement'},
    'TT_pos_x': {'units': 'm',
                 'description': 'Tower top fore-aft displacement (pos. x)'},
    'TT_pos_y': {'units': 'm',
                 'description': 'Tower top side-side displacement (pos. y)'},
    'TT_acc_x': {'units': 'm/s^2',
                 'description': 'Tower top fore-aft aceleration'},
    'TT_acc_y': {'units': 'm/s^2',
                 'description': 'Tower top side-side aceleration'},
    # Is this in the local or global coordinate???
    'Bld1_tip_pos_x': {
        'units': 'm',
        'description': 'Blade 1 tip position in x_b (Blade coordinate system)'
        },
    'Bld1_tip_pos_y': {
        'units': 'm',
        'description': 'Blade 1 tip position in y_b (Blade coordinate system)'
        },
    'Bld1_tip_pos_z': {
        'units': 'm',
        'description': 'Blade 1 tip position in z_b (Blade coordinate system)'
        },
    'Bld2_tip_pos_x': {
        'units': 'm',
        'description': 'Blade 2 tip position in x_b (Blade coordinate system)'
        },
    'Bld2_tip_pos_y': {
        'units': 'm',
        'description': 'Blade 2 tip position in y_b (Blade coordinate system)'
        },
    'Bld2_tip_pos_z': {
        'units': 'm',
        'description': 'Blade 2 tip position in z_b (Blade coordinate system)'
        },
    'Bld3_tip_pos_x': {
        'units': 'm',
        'description': 'Blade 3 tip position in x_b (Blade coordinate system)'
        },
    'Bld3_tip_pos_y': {
        'units': 'm',
        'description': 'Blade 3 tip position in y_b (Blade coordinate system)'
        },
    'Bld3_tip_pos_z': {
        'units': 'm',
        'description': 'Blade 3 tip position in z_b (Blade coordinate system)'
        },
	'Bld1_tip_gl_pos_x': {
        'units': 'm',
        'description': 'Blade 1 tip position in global coordinate Xg'
        },
    'Bld1_tip_gl_pos_y': {
        'units': 'm',
        'description': 'Blade 1 tip position in global coordinate Yg'
        },
    'Bld1_tip_gl_pos_z': {
        'units': 'm',
        'description': 'Blade 1 tip position in global coordinate Zg'
        },
    'hub1_pos_x': {
        'units': 'm',
        'description': 'Hub center position in global coordinate Xg'
        },
    'hub1_pos_y': {
        'units': 'm',
        'description': 'Hub center position in global coordinate Yg'
        },
    'hub1_pos_z': {
        'units': 'm',
        'description': 'Hub center position in global coordinate Zg'
        },
    'hub1_vel_x': {
        'units': 'm/s',
        'description': 'Hub center velocity in global coordinate Xg'
        },
    'hub1_vel_y': {
        'units': 'm/s',
        'description': 'Hub center velocity in global coordinate Yg'
        },
    'hub1_vel_z': {
        'units': 'm/s',
        'description': 'Hub center velocity in global coordinate Zg'
        },
    'Bld1_Vy': {
        'units': 'm/s',
        'description': 'Longitudinal wind speed of Blade 1 at R=72.3 meters'
        },
    'Bld1_Angle_attack': {
        'units': 'deg',
        'description': 'Blade 1 angle of attack, at radius R=72.3 meters'
        },
    'Bld2_Angle_attack': {
        'units': 'deg',
        'description': 'Blade 2 angle of attack, at radius R=72.3 meters'
        },
    'Bld3_Angle_attack': {
        'units': 'deg',
        'description': 'Blade 3 angle of attack, at radius R=72.3 meters'
        },
    'Bld1_Cl': {
        'units': 'kNm',
        'description': 'Blade 1 lift coefficient, Cl, at dius R=72.3 [m]'
        },
    'Bld2_Cl': {
        'units': 'kNm',
        'description': 'Blade 2 lift coefficient, Cl, at radius R=72.3 [m]'
        },
    'Bld3_Cl': {
        'units': 'kNm',
        'description': 'Blade 3 lift coefficient, Cl, at radius R=72.3 [m]'
        },
    'Bld1_Cd': {
        'units': 'kNm',
        'description': 'Blade 1 drag coefficient, Cd, at radius R=72.3 [m]'
        },
    'Bld2_Cd': {
        'units': 'kNm',
        'description': 'Blade 2 drag coefficient, Cd, at radius R=72.3 [m]'
        },
    'Bld3_Cd': {
        'units': 'kNm',
        'description': 'Blade 3 drag coefficient, Cd, at radius R=72.3 [m]'
        },
    # DLL Channels:
    'DLL_torque_ref': {
        'units': 'Nm', 'description': 'DLL generator torque reference'
        },
    'DLL_bld1_pitch_angle_ref': {
        'units': 'rad', 'description': 'DLL pitch angle reference of blade 1'
        },
    'DLL_bld2_pitch_angle_ref': {
        'units': 'rad', 'description': 'DLL pitch angle reference of blade 2'},
    'DLL_bld3_pitch_angle_ref': {
        'units': 'rad', 'description': 'DLL pitch angle reference of blade 3'},
    'DLL_power_ref': {
        'units': 'W', 'description': 'DLL power reference'},
    'DLL_filter_wsp': {
        'units': 'm/s', 'description': 'DLL filtered wind speed'},
    'DLL_filter_rot_speed': {
        'units': 'rad/s', 'description': 'DLL filtered rotor speed'},  
    'DLL_filter_rot_speed_error_torque': {
        'units': 'rad/s',
        'description': 'DLL filtered rotor speed error for torque'
        },
    'DLL_bandpass_filter_rot_speed': {
        'units': 'rad/s', 'description': 'DLL bandpass filtered rotor speed'},
    'DLL_prop_torque_con': {
        'units': 'Nm',
        'description': 'DLL proportional term of torque controller'},
    'DLL_int_torque_controller': {
        'units': 'Nm',
        'description': 'DLL integral term of torque controller'},
    'DLL_min_lim_torque': {
        'units': 'Nm', 'description': 'DLL minimum limit of torque'},
    'DLL_max_lim_torque': {
        'units': 'Nm', 'description': 'DLL maximum limit of torque'},
    'DLL_torque_lim_switch_pitch': {
        'units': '-', 'description': 'DLL torqye limit switch based on pitch'},
    'DLL_filter_rot_speed_error_pitch': {
        'units': 'rad/s',
        'description': 'DLL filtered rotor speed error for pitch'},
    'DLL_power_error_pitch': {
        'units': 'W', 'description': 'DLL power error for pitch'},
    'DLL_prop_pitch_con': {
        'units': 'rad',
        'description': 'DLL proportional term of pitch controller'},
    'DLL_int_pitch_controller': {
        'units': 'rad',
        'description': 'DLL integral term of pitch controller'},
    'DLL_min_lim_pitch': {
        'units': 'rad', 'description': 'DLL minimum limit of pitch'},
    'DLL_max_lim_pitch': {
        'units': 'rad', 'description': 'DLL maximum limit of pitch'},
    'DLL_torque_ref_damper': {
        'units': 'Nm', 'description': 'DLL torque reference from dt damper'},
    'DLL_status_signal': {
        'units': '-', 'description': 'DLL status signal'},
    'DLL_total_added_pitch_rate': {
        'units': 'rad/s', 'description': 'DLL total added pitch rate'},
    'DLL_filter_mean_pitch': {
        'units': 'rad',
        'description': 'DLL filtered mean pitch for gain schedule'
        },
    'DLL_flag_mech_brake': {
        'units': '-',
        'description': 'DLL flag for mechanical brake [0=off / 1=on]'
        },
    'DLL_flag_emergency_pitch_stop': {
        'units': '-',
        'description': 'DLL flag for emergency pitch stop [0=off / 1=on]'
        },
    'DLL_lp_filter_acc_level': {
        'units': 'm/s^2',
        'description': 'DLL lp filtered acceleration level'
        },
    'DLL_mon_avg_ref_pitch': {
        'units': 'rad',
        'description': 'DLL monitored average of reference pitch'
        },
    'DLL_mon_avg_actual_pitch_blade1': {
        'units': 'rad',
        'description': 'DLL monitored average of actual pitch for blade 1'
        },
    'DLL_mgen_lss': {
        'units': 'Nm', 'description': 'DLL generator input: LSS '},
    'DLL_pelec': {
        'units': 'W', 'description': 'DLL generator input: Electrical power'},
    'DLL_mframe': {
        'units': 'Nm', 'description': 'DLL generator input: Mframe'},
    'DLL_mgen_hss': {
        'units': 'Nm', 'description': 'DLL generator input: Mgen HSS'},
    'DLL_grid_flag': {
        'units': '-',
        'description': 'DLL generator input: Grid flag [0=run/1=stop]'},
    'DLL_brake_torque': {
        'units': 'Nm', 'description': 'DLL mechanical brake: Brake torque'},
    'DLL_pitch1': {'units': 'rad', 'description': 'DLL pitch servo: Pitch 1'},
	'DLL_pitch2': {'units': 'rad', 'description': 'DLL pitch servo: Pitch 2'},
	'DLL_pitch3': {'units': 'rad', 'description': 'DLL pitch servo: Pitch 3'},
	'DLL_Bld_tip_tow': {
        'units': 'm',
        'description': 'DLL check tower clearence: min. distance bladetips'
        ' tower'
        }
	}


# List variable names for Hub-lidar data
var_names_HuLi = list(HuLi_metadata.keys())

# List variables names for aeroelastic response of wind turbine
var_names_aero = list(aero_metadata.keys())
