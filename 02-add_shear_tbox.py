# -*- coding: utf-8 -*-
"""
@author: espa

Generates a turbulence box with shear and saves it in the same format as HAWC2.
The htc files for the HAWC2 simulation will read this turbulence box, as the 
longitudinal component.

This is performed manually in Python, due to the shear is added at hub height (Zh = -119 m). 
However, in HAWC2, the turbulence box is center at Zg = -200 m. 
"""

import numpy as np
import hulidb.turbbox as tbox
import hulidb.functions_NETcdf as fNet
from _ltb_values import kw_turbgen

# %% Read original turbulence box:
    
Cases = fNet.generate_Cases(**kw_turbgen)

for case in Cases:
    path = '/dtu_10mw/turb/'
    box = tbox.TurbBox(path, case, 'power_law', opt='hawc2', 
                       **kw_turbgen)
    
    box.df_to_h2turb_shear()
    print(f'Turbulence box with shear has been saved.')

    # Checking creation:
    # New turbulence box:
    path_turb = './dtu_10mw/turb/'
    box_new = tbox.read_windsimu(path_turb, case, 
                                 kw_turbgen['Nx'],
                                 kw_turbgen['Ny'],
                                 kw_turbgen['Nz'], 
                                 opt='u', shear=True)
        
    # Original turbulence Box:
    u_shear = box.u
    print(np.array_equal(box_new, u_shear))
    
