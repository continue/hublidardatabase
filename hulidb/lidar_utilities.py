# -*- coding: utf-8 -*-
"""
@author: espa

Functions for lidar utilities, to extract hub-lidar data from HAWC2 results.
This module is used for testing the new implementation compared with the 
previous one.

"""
import numpy as np
import pandas as pd
import os
from wetb.hawc2.Hawc2io import ReadHawc2
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt

# %% Function to find Lidar cases:


def find_df_cases(df_config, df_lidar):
    """
    To find the cases based on lidar configurations, creating a DataFrame
    that provides all the beams, and index for HAWC2 result search.

    Parameters
    ----------
    df_config : dict
        Based on excel file that containst desire configurations for lidar
        to be studied.

    Returns
    -------
    df_cases : dict
        Containts all the configuration cases, with all the information for
        each case, including the index on the htc file, which will help
        to extract the data from HAWC2 results.

    """
    df_cases = {}
    for case in df_config.columns:
        i = 0
        df_new = pd.DataFrame()
        for beam in range(int(len(df_config)/5)):
            if pd.isna(df_config[case][i+3]):
                continue
            else:
                ranges = np.linspace(
                    int(df_config[case][i+3]),
                    int(df_config[case][i+4]),
                    int(df_config[case][i+2]))
            for elem in ranges:
                elem = int(elem)
                # Inside each case, the first is to
                # check the theta:
                idx = (df_lidar.index[(
                    df_lidar['theta'] == df_config[case][i]) &
                    (df_lidar['psi'] == df_config[case][i+1]) &
                    (df_lidar['Focus-Length'] == elem)])
                if idx.empty:
                    continue
                else:
                    temp_df = df_lidar.loc[idx]
                    temp_df.insert(0, 'beam',
                                   'b' + str(beam+1))
                df_new = pd.concat([df_new, temp_df])
            i = i + 5
        df_cases[case] = df_new
    return df_cases

# %% Function to translate x hawc2 to y user reference coordinate system:


def transf_xh2toyref(x_hawc2, **arg):
    """
    To transform the x HAWC2 coordinate system to Y user
    reference coordinate system, which goes from
    Y: 0 to Turbulence Box Width.

    Parameters
    ----------
    x_hawc2 : array
        array from HAWC2 results for the values on x HAWC2
        coordinate system.
    arg : dict
        Will provide the width, to know from where the
        coordinate needs to be translated for the new
        coordinate been from 0 to Width.

    Returns
    -------
    yref : array
        Values in Y reference user coordinate system.

    """

    if isinstance(x_hawc2, tuple):
        x_hawc2 = x_hawc2[0]
    #yref = x_hawc2 + arg['tb_wid']/2
    yref = (-1) * x_hawc2  # To keep at the center, but in the opossited direction
    return yref

# %% Function to translate y hawc2 to x user reference coordinate system:


def transf_yh2toxref(y_hawc2, **arg):
    """
    To transform the Y HAWC2 coordinate system to X user reference
    coordinate system, changing the direction and moving the positions to
    account for the shaft length for the wind turbine.

    Parameters
    ----------
    y_hawc2 : array
        array from HAWC2 results for the values on y HAWC2
        coordinate system (wind direction).
    arg : dict
        Will provide the shaft_length to correct the position to the zero
        user reference coordinate system.

    Returns
    -------
    xref : array
        Values in X reference user coordinate system, which represents the
        wind direction.

    """
    if isinstance(y_hawc2, tuple):
        y_hawc2 = y_hawc2[0]
    xref = ((y_hawc2 + arg['shaft_length']) * (-1))
    return xref

# %% Function to translate x hawc2 to y user reference coordinate system:


def transf_zh2tozref(z_hawc2, **arg):
    """
    To transform the Z HAWC2 coordinate system to Z user
    reference coordinate system, where the Hub Height is
    moved from -90[m] to tb_ht/2 (65 [m]).

    Parameters
    ----------
    z_hawc2 : array
        array from HAWC2 results for the values on z HAWC2
        coordinate system.
    arg : dict
        Will provide the hub height and the height of the
        turbulence box, to translate the to the user
        coordinate system.

    Returns
    -------
    zref : array
        Values in Z reference user coordinate system.

    """
    if isinstance(z_hawc2, tuple):
        z_hawc2 = z_hawc2[0]
    #zref = (((z_hawc2 - arg['z_hub'])*(-1)) + arg['tb_ht']/2)
    zref = -z_hawc2
    return zref

# %% Function to translate x ref to xt user reference coordinate system:


def transf_xtoxt(xref, wsp, t_hf):
    """
    To transform the xref user coordinate system to xt user
    reference coordinate system, where xref is combined
    with time.

    Parameters
    ----------
    x_ref : array
        array x from user reference coordinate.
    wsp : float
        Wind Speed [m/s]
    t_hf : array
        array with the time simulation.

    Returns
    -------
    xt : array
        Location in new coordinate system based on xref
        location and time step.

    """
    xt = xref + (wsp * t_hf)
    return xt

# %% Function to extract Lidar Data from HAWC2 results:


def extract_lidar_res(fname, sampling_freq, df_cases, t_start, t_end, **kw):
    """
    Extracts the lidar measurements for each beam based on the lidar
    configuration in df_cases. It provides all the values based on the
    sampling frequency.

    Parameters
    ----------
    wsp : float
        Wind Speed [m/s].
    fname : str
        Name of the case.
    smapling_freq : float
        Sampling frequency for the lidar in Hz.
    df_cases : dict
        Contains all the lidar configurations in study.
    **kw : dict
        Dictionary with general information for the path where HAWC2 results
        and channel idx for lidar data in HAWC2.

    Returns
    -------
    Lidar_Measurements : dict
        HAWC2 results and additional information for each lidar configuration
        values, sampled based on sampling frequency depending on the number of
        beams.

    """
    
    # To extract wind speed from the selected case:
    part_before_seed = fname.split('_seed')[0]
    wind_speed_str = part_before_seed.split('wsp_')[1]
    wsp = float(wind_speed_str)
    
    
    Lidar_Measurements = {}
    t_scan = 1/sampling_freq
    hawc2_path = kw['path_res'] + fname + '.hdf5'
    read_h2 = ReadHawc2(hawc2_path)
    hawc2_res = read_h2.ReadGtsdf()
    t_hf = hawc2_res[:, 0]
    # For the raw Hub-mounted Lidar Data:
            
    for key, arr in df_cases.items():
                   
        df_cases_unique = np.unique(arr['beam'])
        theta_config = arr.groupby('beam')['theta'].first().to_numpy()
        psi_config = arr.groupby('beam')['psi'].first().to_numpy()
        HM_Lidar = {}
        
        for i in range(len(df_cases_unique)):
            # Sampling Time for each beam:
            t_scan_seq = np.arange(
                t_hf[0]+(t_scan * i),
                t_hf[-1]+(t_scan * i) + 1e-6,
                t_scan*len(df_cases_unique))
            
            # Here I get the index for this beam:
            aidx = (arr['beam'].index[
                arr['beam'] == df_cases_unique[i]])
            # Index of HAWC2 results for each beam:
            idx_hawc2 = (aidx * 8) + kw['init_idx_lidar']
            # Fixing idx_time issue:
            #idx_time = np.where(np.in1d(t_hf, t_scan_seq))[0]
            
            tolerance = 1e-6  # Define a tolerance level for floating-point comparisons
            matching_indices = np.where(np.abs(t_hf[:, np.newaxis] - t_scan_seq) < tolerance)
            idx_time = matching_indices[0]
            
            Data_Lidar = pd.DataFrame()
            
            for elem in idx_hawc2:
                
                # Time:
                t_hawc2 = (pd.DataFrame(
                    hawc2_res[:, 0]).iloc[
                        idx_time, :].rename(
                            columns={0: 't_hawc2'}))
                
                # Shaft rotational angle in degrees:
                omega = (pd.DataFrame(
                    hawc2_res[:, 1]).iloc[
                         idx_time, :].rename(
                             columns={0: 'omega'}))
                # Free wind Vy:
                FreeWind_Vy = (pd.DataFrame(
                    hawc2_res[:, 14]).iloc[
                         idx_time, :].rename(
                             columns={0: 'FreeWind_Vy'}))

                # Average Rotor wind speed from hawc2:                             
                AvgRotWsp = (pd.DataFrame(
                    hawc2_res[:, 16]).iloc[
                         idx_time, :].rename(
                             columns={0: 'AvgRotWsp'}))
                
                # Hub-lidar values:
                x_hawc2 = (pd.DataFrame(
                    hawc2_res[:, elem]).iloc[
                        idx_time, :].rename(
                            columns={0: 'x_hawc2'}))
                y_hawc2 = (pd.DataFrame(
                    hawc2_res[:, elem+1]).iloc[
                        idx_time, :].rename(
                            columns={0: 'y_hawc2'}))
                z_hawc2 = (pd.DataFrame(
                    hawc2_res[:, elem+2]).iloc[
                        idx_time, :].rename(
                            columns={0: 'z_hawc2'}))
                LOS_wsp = (pd.DataFrame(
                    hawc2_res[:, elem+3]).iloc[
                        idx_time, :].rename(
                            columns={0: 'LOS_wsp'}))
                Nom_wsp = (pd.DataFrame(
                    hawc2_res[:, elem+4]).iloc[
                        idx_time, :].rename(
                            columns={0: 'Nom_wsp'}))
                            
                u_nom = (pd.DataFrame(
                    hawc2_res[:, elem+5]).iloc[
                        idx_time, :].rename(
                            columns={0: 'v_comp'}))
                            
                v_nom = (pd.DataFrame(
                    hawc2_res[:, elem+6]).iloc[
                        idx_time, :].rename(
                            columns={0: 'u_comp'}))
                            
                w_nom = (pd.DataFrame(
                    hawc2_res[:, elem+7]).iloc[
                        idx_time, :].rename(
                            columns={0: 'w_comp'}))
                            
                beam_no = np.full((len(hawc2_res[:, 0]), 1),
                                  df_cases_unique[i])
                
                beam_no = (pd.DataFrame(
                    beam_no).iloc[
                        idx_time, :].rename(
                            columns={0: 'Beam No'}))
                            
                theta = np.full((len(hawc2_res[:, 0]), 1),
                                  theta_config[i])
                
                theta = (pd.DataFrame(
                    theta).iloc[
                        idx_time, :].rename(
                            columns={0: 'Theta'}))
                            
                psi = np.full((len(hawc2_res[:, 0]), 1),
                                   psi_config[i])
                 
                psi = (pd.DataFrame(
                          psi).iloc[
                          idx_time, :].rename(
                          columns={0: 'psi'}))           
                                                       
                # y = y_hawc2 - (wsp * t_hf)
                # Based on functions created for this:
                xref = transf_yh2toxref(hawc2_res[:, elem+1], **kw)
                yref = transf_xh2toyref(hawc2_res[:, elem], **kw)
                zref = transf_zh2tozref(hawc2_res[:, elem+2], **kw)
                xt = transf_xtoxt(xref, wsp, hawc2_res[:, 0])
                xref = (pd.DataFrame(
                    xref).iloc[
                        idx_time, :].rename(
                            columns={0: 'xref'}))
                yref = (pd.DataFrame(
                    yref).iloc[
                        idx_time, :].rename(
                            columns={0: 'yref'}))
                zref = (pd.DataFrame(
                    zref).iloc[
                        idx_time, :].rename(
                            columns={0: 'zref'}))
                xt = (pd.DataFrame(xt).iloc[
                        idx_time, :].rename(
                            columns={0: 'xt'}))
                                            
                Data = pd.concat([t_hawc2,
                                  omega,
                                  FreeWind_Vy,
                                  AvgRotWsp,
                                  x_hawc2,
                                  y_hawc2,
                                  z_hawc2,
                                  LOS_wsp,
                                  Nom_wsp,
                                  u_nom,
                                  v_nom,
                                  w_nom,
                                  xref,
                                  yref,
                                  zref,
                                  xt,
                                  beam_no,
                                  theta,
                                  psi], axis=1)
                Data_Lidar = pd.concat([Data_Lidar, Data], axis=0)
                
                Data_Lidar = Data_Lidar[(Data_Lidar['t_hawc2'] >= t_start) & (Data_Lidar['t_hawc2'] <= t_end)]
                
            key_beam = df_cases_unique[i]
            HM_Lidar[key_beam] = Data_Lidar
        Lidar_Measurements[key] = HM_Lidar
    return Lidar_Measurements

# %% Function for Rectangular prism for 3d Projection:


def make_rec_prism(tw_plot, th_plot, tb, th, z_hub, xt_max):
    """
    Generates edges and vertices for the plot of a
    rectangular prims, to generate a 3D plot with the lidar
    measurements.
    It is based on the user coordinate system.
    """
    # Define Initial positions:
    x0 = 0
    y0 = -tb/2
    z0 = z_hub - th/2
    w_max = tb/2
    h_max = z_hub + th/2
    
    # Define the edges of the cube
    edges = [(0, 1), (1, 2), (2, 3), (3, 0),
             (4, 5), (5, 6), (6, 7), (7, 4),
             (0, 4), (1, 5), (2, 6), (3, 7)]
    # Define vertices for the cube:
    vertices = np.array([[x0, y0, z0],
                         [xt_max, y0, z0],
                         [xt_max, w_max, z0],
                         [x0,  w_max, z0],
                         [x0, y0,  h_max],
                         [xt_max, y0,  h_max],
                         [xt_max,  w_max,  h_max],
                         [x0,  w_max,  h_max]])
    return edges, vertices

# %% Function to plot 3D projection of Lidar measurements:


def plot_3DLidar(path, file_name, D, z_hub, center, tb_w_plot, tb_h_plot, 
                 tb_w_box, tb_h_box, 
                 xt_max, x2, y2, z2, u,
                 title,
                 angle, dash_line, opt='3d', format='eps'):
    """
    Plot a 3D projection of the lidar measurements as scatter
    for a turbulence box with a length define by the user.

    Parameters
    ----------
    D : float
        Rotor diameter for the wind turbine model [m].
    tb_w : float
        Width of turbulence box [m].
    tb_h : float
        Heigth of turbulence box [m].
    xt_max : float
        Length of turbulence box [m].
    DF_lidar : dict
        DataFrame that containst measuremnents from HAWC2
        results already proceed based on sampling frequency
        and lidar configuration selected.
    dash_line : bool
        True is dash_line at rotor center must be drawn, or False if not.

    """
    # Creating rotor plane + dash line at center for pretty plots:
    x, y, z = make_rotorplane(D, z_hub, tb_w_plot, 
                              dash_line=False,
                              xt_min=0, xt_max=1000)
    # Creating rectangular prism edges:
    edges, vertices = make_rec_prism(tb_w_plot, tb_h_plot,
                                     tb_w_box, 
                                     tb_h_box, z_hub, xt_max)
    # Creating Figure:
    fig = plt.figure(figsize=(10, 10))
    ax = fig.add_subplot(111, projection='3d')

    img = ax.scatter(x2, y2, z2, c=u, cmap=plt.viridis(), alpha=0.7)
    # Moving the colorbar to a better position:
    cbar_ax = fig.add_axes([0.85, 0.15, 0.05, 0.5])
    cbar = plt.colorbar(img, cax=cbar_ax)
    cbar.set_label('Wind Speed [m/s]', fontsize=12, fontweight='bold')   
    cbar_ax.set_position([0.9, 0.15, 0.03, 0.5])

    # Plot the cube
    box_color = 'black'  # Choose a contrasting color
    line_width = 2  # Increase line width for visibility
    zorder_box = 3  # Higher zorder to plot on top
    
    ax.get_proj = lambda: np.dot(Axes3D.get_proj(ax),
                                 np.diag([1.5, 0.5, 0.5, 0.8]))
    # ax.plot(x_lin, y_lin, z_lin, color='blue', linestyle='dashed',
    #         linewidth=line_width, zorder=zorder_box)
    ax.plot(x, y, z, color='black', linestyle='dashed',
            linewidth=line_width, zorder=zorder_box)
    
    #Plot the cube    
    for edge in edges:
        ax.plot([vertices[edge[0], 0], vertices[edge[1], 0]],
                [vertices[edge[0], 1], vertices[edge[1], 1]],
                [vertices[edge[0], 2], vertices[edge[1], 2]],
                color=box_color, linewidth=line_width, linestyle='dashed',
                zorder=zorder_box)
        
    ax.set_title(title, fontsize=14, 
                 fontweight="bold")
    
    ax.set_ylim(ax.get_ylim()[::-1])
    
    if opt=='frontal':
        ax.set_xticks([])
        ax.set_xticklabels([])
        ax.set_ylabel('Y [m]', fontsize=12, fontweight="bold", labelpad=12)
        ax.set_zlabel('Z [m]', fontsize=12, fontweight="bold", labelpad=12)
        
    elif opt=='3d':
        ax.set_xlabel('X [m]', fontsize=12, fontweight='bold', labelpad=12)  # Adjust labelpad as needed
        ax.set_ylabel('Y [m]', fontsize=12, fontweight="bold")
        ax.set_zlabel('Z [m]', fontsize=12, fontweight="bold")
        
    ax.view_init(elev=angle[0], azim=angle[1])
    ax.set_box_aspect((3, 3, 3))
    
    #plt.show()
    
    ax.grid(False)
    
    if not os.path.exists(path):
        os.makedirs(path)
    
    # To save the Figure:
    full_path = f"{path}/{file_name}.{format}"
    
    # Save the figure
    plt.savefig(full_path, format=format)
        
    # Print a message to indicate the figure has been saved
    print(f"Figure has been saved in: {full_path}")
    
    return

# %%


def plot_3DLidar_front(path, file_name, D, z_hub, center, tb_w_plot, tb_h_plot, 
                 tb_w_box, tb_h_box, 
                 xt_max, x2, y2, z2, u,
                 title,
                 angle, dash_line, opt='3d', format='eps'):
    """
    Plot a 3D projection of the lidar measurements as scatter
    for a turbulence box with a length define by the user.

    Parameters
    ----------
    D : float
        Rotor diameter for the wind turbine model [m].
    tb_w : float
        Width of turbulence box [m].
    tb_h : float
        Heigth of turbulence box [m].
    xt_max : float
        Length of turbulence box [m].
    DF_lidar : dict
        DataFrame that containst measuremnents from HAWC2
        results already proceed based on sampling frequency
        and lidar configuration selected.
    dash_line : bool
        True is dash_line at rotor center must be drawn, or False if not.

    """
    # Creating rotor plane + dash line at center for pretty plots:
    x, y, z = make_rotorplane(D, z_hub, tb_w_plot, 
                              dash_line=False,
                              xt_min=0, xt_max=1000)
    # Creating rectangular prism edges:
    edges, vertices = make_rec_prism(tb_w_plot, tb_h_plot,
                                     tb_w_box, 
                                     tb_h_box, z_hub, xt_max)
    # Creating Figure:
    fig = plt.figure(figsize=(10, 10))
    ax = fig.add_subplot(111, projection='3d')

    img = ax.scatter(x2, y2, z2, c=u, cmap=plt.viridis(), alpha=0.7)
    # Moving the colorbar to a better position:
    cbar_ax = fig.add_axes([0.85, 0.15, 0.05, 0.5])
    cbar = plt.colorbar(img, cax=cbar_ax)
    cbar.set_label('Wind Speed [m/s]', fontsize=12, fontweight='bold')   
    cbar_ax.set_position([0.9, 0.15, 0.03, 0.5])

    # Plot the cube
    box_color = 'black'  # Choose a contrasting color
    line_width = 2  # Increase line width for visibility
    zorder_box = 3  # Higher zorder to plot on top
    
    ax.get_proj = lambda: np.dot(Axes3D.get_proj(ax),
                                 np.diag([1.5, 0.5, 0.5, 0.8]))
    # ax.plot(x_lin, y_lin, z_lin, color='blue', linestyle='dashed',
    #         linewidth=line_width, zorder=zorder_box)
    ax.plot(x, y, z, color='red', linestyle='dashed',
            linewidth=line_width, zorder=zorder_box)
    
    # Plot the cube    
    for edge in edges:
        ax.plot([vertices[edge[0], 0], vertices[edge[1], 0]],
                [vertices[edge[0], 1], vertices[edge[1], 1]],
                [vertices[edge[0], 2], vertices[edge[1], 2]],
                color=box_color, linewidth=line_width, linestyle='dashed',
                zorder=zorder_box)
        
    ax.set_title(title, fontsize=14, 
                 fontweight="bold")
    
    ax.set_ylim(ax.get_ylim()[::-1])
    
    if opt=='frontal':
        ax.set_xticks([])
        ax.set_xticklabels([])
        ax.set_ylabel('Y [m]', fontsize=12, fontweight="bold", labelpad=12)
        ax.set_zlabel('Z [m]', fontsize=12, fontweight="bold", labelpad=12)
        
    elif opt=='3d':
        ax.set_xlabel('X [m]', fontsize=12, fontweight='bold', labelpad=12)  # Adjust labelpad as needed
        ax.set_ylabel('Y [m]', fontsize=12, fontweight="bold")
        ax.set_zlabel('Z [m]', fontsize=12, fontweight="bold")
        
    ax.view_init(elev=angle[0], azim=angle[1])
    ax.set_box_aspect((3, 3, 3))
    
    #plt.show()
    
    ax.grid(False)
    
    if not os.path.exists(path):
        os.makedirs(path)
    
    # To save the Figure:
    full_path = f"{path}/{file_name}.{format}"
    
    # Save the figure
    plt.savefig(full_path, format=format)
        
    # Print a message to indicate the figure has been saved
    print(f"Figure has been saved in: {full_path}")
    
    return

# %% Function to generate rotor plane drawing:


def make_rotorplane(D, z_hub, tb_w, dash_line=False, xt_min=0, xt_max=0):
    """
    Generates x, y,z to plot a rotor plane in the plot.
    
    Parameters
    ----------
    D : float
        Rotor diameter [m]
    tb_w : float
        Width of turbulence box

    Returns
    -------
    x : array
        Values in the x coordinate system
    y : array
        Values in the y coordinate system
    z : array
        Values in the z coordinate system

    """
    if dash_line is False:
        theta = np.linspace(0, 2 * np.pi, tb_w)
        y = (D/2 * np.cos(theta))
        x = np.zeros(tb_w)
        z = ((D/2 * np.sin(theta)) + (z_hub))
        return x, y, z
    
    elif dash_line is True:
        if (xt_min, xt_max) is float or int:
            x_lin = np.linspace(xt_min, xt_max, 150)
            y_lin = (np.ones(tb_w + 100)*(tb_w/2))
            z_lin = (np.ones(tb_w + 100)*(tb_w/2))
            theta = np.linspace(0, 2 * np.pi, tb_w)
            y = (D/2 * np.cos(theta) + (tb_w/2))
            x = np.zeros(tb_w)
            z = ((D/2 * np.sin(theta)) + (tb_w/2))
        else:
            raise TypeError('xt_min and xt_max must be float or int type.')
        return x, y, z, x_lin, y_lin, z_lin
    else:
        raise TypeError('dash_line argument must be True or False.')


# %% 


    