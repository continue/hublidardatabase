# -*- coding: utf-8 -*-
"""
@author: espa

General functions across all different modules.
"""

import os
import re

# %% Checks if options provided are valid: 

def check_allow_opt(opt, allowed_options):
    """
    Check if the provided option is within the allowed options.

    Parameters
    ----------
    opt : str
        Option to be checked.
    allowed_options : list
        List with the allowed options.
    
    Returns
    -------
    None

    Raises
    ------
    ValueError
        If the provided option is not within the allowed options.

    """

    # Check if the provided option is within the allowed options
    if opt not in allowed_options:
        # If not, raise a ValueError with a msg informing the valid options
        raise ValueError(f"Invalid option '{opt}'. Allowed options are: {allowed_options}.")
    
    return None


# %% Check if path exist, if not create path:


def ensure_path_exists(path):
    """Ensure that a directory exists. If the path does not exist, create it."""
    if not os.path.exists(path):
        os.makedirs(path)
        print(f"Path created: {path}")
    else:
        print(f"Path already exists: {path}")
        

    
# %% Function to change directory to work in slurm cluster

def transform_path(relative_path):
    """Transform a relative path into an absolute path based on the current execution directory.
       Automatically appends a directory separator to directory paths."""

    if relative_path.startswith('./'):
        relative_path = relative_path[2:]  # Remove './' from the start of the path

    # Ensure the relative path ends with a separator if you expect it to be a directory
    if not relative_path.endswith(os.sep):
        relative_path += os.sep

    CURRENT_DIR = os.path.dirname(__file__)
    PROJECT_ROOT = os.path.abspath(os.path.join(CURRENT_DIR, os.pardir))
    base_path = os.path.join(PROJECT_ROOT, relative_path)
    full_path = os.path.normpath(base_path)  # Normalize path to fix any mixed separators

    # Append separator to the normalized path if it's determined to be a directory path
    if not full_path.endswith(os.sep) and os.path.isdir(base_path):
        full_path += os.sep

    return full_path


# %%


def check_files_and_convention(folder_path):
    
    # Check for .nc files in the specified folder
    nc_files = [f for f in os.listdir(folder_path) if f.endswith('.nc')]
    
    # List to store filenames without extension
    filenames = [os.path.splitext(f)[0] for f in nc_files]
            
    return filenames

# Example usage
folder_path = './NETcdf/'
filenames = check_files_and_convention(folder_path)
print("Filenames:", filenames)
    
