"""
@author: espa

Functions for the hub-lidar database generation. Generates hub-lidar
possible configurations based on initial parameters, and generates the htc
files for turbulence box generation and for aeroelastic simulation in HAWC2.

"""

import os
import numpy as np
import pandas as pd
import hulidb.functions as func
from wetb.hawc2 import HTCFile
from wetb.wind.turbulence.mann_parameters import var2ae

# To locate Mann generator executable:
CURRENT_DIR = os.path.dirname(__file__)
PROJECT_ROOT = os.path.abspath(os.path.join(CURRENT_DIR,
                                            os.pardir))
MANN_BOX_FMT = '<f'  # Binary turbulence datatype

# %%  Function that creates htc lines for Lidar Configurations


def create_lidar_config_htc(hub_height, lidar_arg, test=False):
    """
    Generates DataFrame with the outputs beam channels in HAWC2, based on all
    possible combinations with the initial inputs from dict: lidar_arg.

    Parameters
    ----------
    arg : dict
        Containst lidar initial information from where the multiple
        configurations are generated, base on the following arrays:
            theta: Half-cone angle (array)
            psi: Azimuthal angle (array)
            ranges: range length for the lidar (array)

    Returns
    -------
    df : DataFrame
        Configurations for Hub-mounted lidar with HAWC2 output structure.

    """
    # To select the proper values from dictionary, based on if test option
    if test == False:
        theta_ang = lidar_arg['theta']
        ranges_hl = lidar_arg['ranges']
        psi_ang = lidar_arg['psi']
        file_name = lidar_arg['config_file']
    else: 
        print(f'Test has been selected.')
        theta_ang = lidar_arg['theta_test']
        ranges_hl = lidar_arg['ranges_test']
        psi_ang = lidar_arg['psi_test']
        file_name = lidar_arg['config_file_test']
    
    path_save = PROJECT_ROOT + lidar_arg['config_path']
    angle_pros = []
    range_pros = []
    cone_angle = []
    beta_angle = []
    height = []
    for elem in theta_ang:
        for elem_range in ranges_hl:
            for psi in psi_ang:
                angle_pros.append(elem)
                range_pros.append(elem_range)
                cone_angle.append(np.arctan(np.radians(elem))*elem_range)
                beta_angle.append(psi)
                height.append(((np.sin(np.radians(elem)) * elem_range) +
                               abs(hub_height)))
    # Creating lidar combinations based on the parameters in argument:
    combinations_lidar = {
        'Height': height,
        'Beggining': lidar_arg['htc_begin'],
        'theta': angle_pros,  # Half-cone Angle
        'psi': beta_angle,  # Azimuthal Angle
        'Focus-Length': range_pros,
        'Range-Length': lidar_arg['deltaP'],
        'Lidar Beam Full width at half Maximum': lidar_arg['deltaL'],
        'Half-width integration volumene': lidar_arg['dv'],
        'Number of integrations': lidar_arg['N_int']
        }
    df = pd.DataFrame(combinations_lidar)
    # To ensure the combinations are inside turbulence box:
    df = (df[(df.Height >= lidar_arg['min_height']) &
             (df.Height <= lidar_arg['max_height'])])
    # Removing the configurations that are redundant:
    index_positions = np.where((df['theta'] == 0) & (df['psi'] != 0))[0]
    df = df.drop(index=index_positions)
    df = df.drop(columns=['Height'])
    beam_number = np.arange(1, len(df)+1, 1)
    Tag = []
    for elem in beam_number:
        Tag.append('# Beam_' + str(elem) + ' ;')
        df['Beam No'] = beam_number
    df['Beam No'] = beam_number
    df['Tag'] = Tag
    print('DataFrame for lidar configurations for HAWC2 htc file completed.\n')
    # To save the DataFrame as *.csv file for later use.
    # Creates the folder if it doesn't exist
    if not os.path.exists(path_save):
        os.makedirs(path_save)
    lidar_file = path_save + file_name
    df.to_csv(lidar_file, index=True, header=True)
    print('DataFrame has been saved at ' + path_save + ' as *.csv file. \n')
    return df



# %% Generates htc files for each turbulence box case:


def make_htc_turb(orig_htc, file_name, df_lidar, idx_start, idx_end, wsp, rot,
                  tstop, turb_name, aed, center_pos, seed, shear_format='none',
                  turb=False, turb_none_exist=False, dxt=None, shear_turb=False, 
                  tstart=0, tower_flex=False,
                  tilt=0, yaw=0, roll=0, free_wind=False, path_htc=None, **kw):
    """
    Generates .htc files to perform HAWC2 simulation. Depending on the inputs,
    it can be to generate the turbulence boxes, or to generate the hub-lidar
    measurements.

    Parameters
    ----------
    orig_htc : str
        File name of the .htc template for the DTU10MW.
    file_name : str
        File name of the inflow case to be simulated.
    df_lidar : DataFrame
        Contains all the output lines for the hub-lidar sensor in HAWC2.
    idx_start : int
        Initial index for the hub-lidar beam output channel in HAWC2.
    idx_end : int
        Final index for the hub-lidar beam output channel in HAWC2.
    wsp : float
        Average wind speed for the simulation, at hub height, in m/s.
    rot : list
        Provides the angle for the windfield rotation parameter in HAWC2. 
        The default is rot=[0, 0, 0], which mean no windfield rotation.
    tstop : float
        Time to stop the aeroelastic simulation.
    turb_name : str
        File name of the turbulence box file. Similar to the case name.
    aed : float
        alpha-epsilon parameter for the Mann-turbulence box generation. It is
        calculated inside the function using the var2ae function from wetb.
    center_pos : float
        Center vertical location in Zg, for the turbulence box, in the global
        coordinate system.
    seed : int
        Seed number for the turbulence box generation.
    shear_format : str, optional
        Shear profile selection to be added to the inflow in the HAWC2
        simulation. The default is 'none'.
    turb : boleean, optional
        It defines if the simulation will use a turbulence box to run. If true,
        it will add the Mann output into the htc file.
        The default is False.
    turb_none_exist : boolean, optional
        It will add in the Mann block the parameters to generate the turbulence
        box from HAWC2. If True, it adds the Mann-model parameters to generate
        the turbulence box. The default is False.
    tstart : float, optional
        Time from where the output of the aeroelastic simulation is saved in
        the result file. The default is 0.
    tower_flex : boolean, optional
        If True, tower will be defined as flexible for the aeroelastic
        simulation. The default is False.
    tilt : float, optional
        Tilt angle in degrees for the wind turbine model. The default is 0.
    yaw : float, optional
        Yaw angle in degrees for the wind turbine model. The default is 0.
    roll : float, optional
        Roll angle in degrees for the wind turbine model. The default is 0.
    **kw : dict
        Dictionary with all the information for the turbulence box generation.

    Raises
    ------
    TypeError
        Raises an error if the shear profile name is not found.

    Returns
    -------
    None.

    """
    # Check shear options:
    allowed_shear = ['none', 'cte', 'power']
    func.check_allow_opt(shear_format, allowed_shear)

    turb_format = 1  # Mann turbulence box
    tint = 0.1

    Time = kw['Time']
    htc = HTCFile(orig_htc)
    tower_shadow=3

    # Shear profile selection:
    if shear_format=='none':
        shear = 0
        alpha = 0
    elif shear_format=='cte':
        shear = 1
        alpha = 0
    elif shear_format=='power':
        shear = 3
        alpha = kw['alpha']
    else:
        raise TypeError('Input must be none, cte or power.')

    # set simulation time and file names
    htc.set_time(start=tstart, stop=tstop, step=kw['dt_simu'])
    htc.set_name(file_name)
    # set tower flexibility and tilt, roll and yaw
    tower_set = [1, 1] if tower_flex else [1, 2]
    htc.new_htc_structure.main_body.timoschenko_input.set = tower_set
    htc.new_htc_structure.orientation.relative__2.body2_eulerang__2 = [tilt, yaw, roll]
    # Wind Block:
    htc.wind.wsp = wsp  # mean wind speed
    htc.wind.tint = tint  # turbulence intensity
    htc.wind.windfield_rotations = rot
    htc.wind.center_pos0 = [0.0, 0.0, center_pos]
    htc.wind.shear_format = [shear, alpha]  # Constant
    htc.wind.turb_format = turb_format  # none
    htc.wind.tower_shadow_method = tower_shadow
    if turb == True:
        if 'mann' in htc.wind:
            del htc.wind.mann
        mann = htc.wind.add_section('mann')
        if turb_none_exist:  # If not, it call mann generator to generated.
            mann.add_line('create_turb_parameters',
                          [kw['L'], aed, kw['gamma'],
                           seed, kw['HighFreqComp']])
        if shear_turb:
            mann.add_line('filename_u', [f'./turb/{turb_name}_u_shear.bin'])
        else:
            mann.add_line('filename_u', [f'./turb/{turb_name}_u.bin'])

        mann.add_line('filename_v', [f'./turb/{turb_name}_v.bin'])
        mann.add_line('filename_w', [f'./turb/{turb_name}_w.bin'])
        if dxt == None:
            mann.add_line('box_dim_u', [kw['Nx'], wsp * Time / kw['Nx']])
        else: 
            mann.add_line('box_dim_u', [kw['Nx'], dxt])
        mann.add_line('box_dim_v', [kw['Ny'], kw['Ly'] / (kw['Ny']-1)])
        mann.add_line('box_dim_w', [kw['Nz'], kw['Lz'] / (kw['Nz']-1)])
        mann.add_line('dont_scale', [1])
        
    output = htc.output
    for i in range(idx_start, idx_end):
        output.add_sensor('aero', 'hub_lidar', values=[df_lidar['theta'][i],
                                                       df_lidar['psi'][i],
                                                       df_lidar['Focus-Length'][i],
                                                       38.4, 24.75, 3, 200,
                                                       df_lidar['Beam No'][i]],
                                                       comment=df_lidar['Tag'][i])
        
    if free_wind:
        # Add free wind output sensors to get the turbulence box as HAWC2 see it:
        Xg = np.linspace(-kw['Ly']/2, kw['Ly']/2, kw['Ny'])
        Zg = np.linspace(0, -kw['Lz'], kw['Nz'])
        #X2 = np.linspace(-kw['Ly']/2, kw['Ly']/2, 10)
        #Z2 = np.linspace(0, -kw['Lz'], 10)

        for pos_x in Xg:
            for pos_z in Zg:
                output.add_sensor('wind', 'free_wind', values=[1, pos_x, 0, pos_z], 
                                                   comment=f'({pos_x}, 0, {pos_z})')
        # Cross sections generation only:
        #for pos_z in Zg: 
        #    output.add_sensor('wind', 'free_wind', values=[1, 0, 0, pos_z], 
        #                                           comment=f'(0, 0, {pos_z})')
        #for pos_x in Xg:
        #    output.add_sensor('wind', 'free_wind', values=[1, pos_x, 0, -200], 
        #                                           comment=f'({pos_x}, 0, 0)')
        
        # This is to test interpolation:    
        #for pos_z in Z2:
        #    output.add_sensor('wind', 'free_wind', values=[1, 0, 0, pos_z], 
        #                                           comment=f'(0, 0, {pos_z})')
        #for pos_x in X2:
        #    output.add_sensor('wind', 'free_wind', values=[1, pos_x, 0, -119], 
        #                                           comment=f'({pos_x}, 0, 0)')
            

    # Get current date and time
    if path_htc is not None:
        folder_name = path_htc
    elif turb_none_exist:
        folder_name = 'turb_box/'
    else:
        folder_name = 'lidar_simulation/'
    
    base_path = './' + kw['turb_model'] + '/htc/'
    full_path = os.path.join(base_path, folder_name)

    if not os.path.exists(full_path):
        os.makedirs(full_path)

    new_htc = full_path + file_name + '.htc'
    # save the new file
    htc.save(new_htc)

    return print(f'HTC File has been saved as: {new_htc}.')


# %% Generates htc file for turbulence box generation:


def generate_htc_tb(orig_htc, center_pos, **kw_turbgen):
    """
    Generates the htc files of 10 second simulation in HAWC2 to generate the
    turbulence boxes for the simulations. This is done in a previous step,
    since multiple simulations will be run based on the same inflow case.

    Parameters
    ----------
    orig_htc : str
        File name of the htc file template, with format: path + file + .htc.
    center_pos : float
        Vertical location in the HAWC2 global coordinate system where the 
        center of the turbulence box in Zg.
    ***kw_turbgen : dict
        Dictionary that includes all the variables for the turbulence box
        generation, like grid points, dimensions, Mann-model parameters, etc.


    Returns
    -------
    None

    """
    
    for wsp in kw_turbgen['wsps']:    
        for seed in kw_turbgen['Seeds']:
            
            print(f'\nGenerating htc file for wind speed: {wsp}, seed: {seed},'
                  ' with flex tower and tilt: 5 [deg] \n')
            
            rot = [0, 0, 0]   # Rotation to apply yaw in the plane. 
            df_lidar = pd.DataFrame()  # Empty Dataframe, for hub-lidar beams
    
            # Based on the IEC Standard:
            sigma = kw_turbgen['I_ref'] * (0.75 * wsp + 5.6)
            variance = sigma**2
    
            sample_frq = 1/(kw_turbgen['Time']/kw_turbgen['Nx'])  
    
            ae = var2ae(variance, kw_turbgen['L'],
                        kw_turbgen['gamma'], wsp,
                        T=kw_turbgen['Time'],
                        sample_frq=sample_frq, plt=False)
            
            print(f'Calculated alpha_epsilon: {ae:.3f}')
            
            # For Turbulence Box Generation:
            file_name = (kw_turbgen['turb_model'] + '_wsp_' + str(wsp) +
                         '_seed_' + str(seed) + f'_ae_{ae:.3f}')
            turb_name = file_name  # Turbulence box file name
    
            idx_start, idx_end = 0, 0
            t_stop = 10  # 10 seconds simulation for turbulence box generation
    
            make_htc_turb(
                orig_htc, file_name, df_lidar, idx_start, idx_end, wsp, 
                rot, t_stop, turb_name, ae, center_pos, seed, 
                shear_format='none', turb=True, turb_none_exist=True, 
                shear_turb=False, tstart=0, tower_flex=False, 
                tilt=0, yaw=0, roll=0, free_wind=True, **kw_turbgen)
            
    return print(f'Generation of htc files for turbulence box simulations are'
                 ' completed, and can be found in ./dtu_10mw/htc/turb_box/')


        
# %% Generates htc files for HAWC2 simulation for hub-lidar data:


def generate_htc_simu(orig_htc, df_lidar, tilt, center_pos, tstop, lidar_arg,
                      **kw_turbgen):
    """
    Generates the htc files for HAWC2 simulations to generate the hub-lidar
    data, based on the information from Dataframe df_lidar. For one inflow
    case, it will generate multiple files, since it adds 2000 output beams
    per file. 


    Parameters
    ----------
    orig_htc : str
        File name of the htc file template, with format: path + file + .htc.
    df_lidar : DataFrame
        Contains all the combinations of possible configurations for the
        hub-lidar, based on the initial values in the dictionary in script
        _ltb_values.py
    tilt : float
        Tilt angle in degrees for the wind turbine model.
    center_pos : float
        Vertical location in the HAWC2 global coordinate system where the 
        center of the turbulence box in Zg.
    tstop : float
        Time to stop the simulation, and final time step where the data will be
        stored on the HAWC2 results. The value selected is to provided 10-min
        simulation, from 100.05 to 700.05 seconds.
    ***kw_turbgen : dict
        Dictionary that includes all the variables for the turbulence box
        generation, like grid points, dimensions, Mann-model parameters, etc.


    Returns
    -------
    None

    """

    for wsp in kw_turbgen['wsps']:    
        for seed in kw_turbgen['Seeds']:
            
            print(f'\nGenerating htc file for wind speed: {wsp}, seed: {seed},'
                  ' with flexible tower and tilt: {tilt} [deg] \n')
            
            rot = [0, 0, 0]   # Rotation to apply yaw in the plane.

            # Based on the IEC Standard:
            sigma = kw_turbgen['I_ref'] * (0.75 * wsp + 5.6)
            variance = sigma**2

            sample_frq = 1/(kw_turbgen['Time']/kw_turbgen['Nx'])  

            ae = var2ae(variance, kw_turbgen['L'],
                        kw_turbgen['gamma'], wsp,
                        T=kw_turbgen['Time'],
                        sample_frq=sample_frq, plt=False)

            print(f'Calculated alpha_epsilon: {ae:.3f}')

            # For Hub-lidar Data simulation:
            idx_start = 0
            idx_end = lidar_arg['beamno_htc']
            total_simulations = (len(df_lidar) // idx_end) + 1
    
            for i in range(total_simulations):
                if idx_end > len(df_lidar):
                    idx_end = len(df_lidar)
    
                fname = (kw_turbgen['turb_model'] + '_wsp_' + str(wsp) + '_seed_' +
                         str(seed) + f'_ae_{ae:.3f}')
                file_name = fname + f'_part_{i+1}'
                turb_name = fname

                make_htc_turb(
                    orig_htc, file_name, df_lidar, idx_start, idx_end, wsp, 
                    rot, tstop, turb_name, ae, center_pos, seed, 
                    shear_format='none', turb=True, turb_none_exist=False, 
                    shear_turb=True, tstart=100, tower_flex=True,
                    tilt=tilt, yaw=0, roll=0, **kw_turbgen)

                idx_start = idx_end
                idx_end = idx_start + lidar_arg['beamno_htc']
                
    return print(f'Generation of htc files for dataset simulation are'
                 ' completed, and can be found in '
                 './dtu_10mw/htc/lidar_simulation/')
