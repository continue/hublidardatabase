# -*- coding: utf-8 -*-
"""
@author: espa

Functions for turbulence box generation, read and save. 
It contains turbox class. 
"""

import os
import numpy as np
import pandas as pd
import xarray as xr
import matplotlib.pyplot as plt
import hulidb.functions as func
import hulidb.functions_NETcdf as fNet
from wetb.hawc2.Hawc2io import ReadHawc2

MANN_BOX_FMT = '<f'  # binary turbulence datatype

# To locate Mann generator executable:
CURRENT_DIR = os.path.dirname(__file__)
PROJECT_ROOT = os.path.abspath(os.path.join(CURRENT_DIR, os.pardir))

# %% Function to read u, v, w binary files HAWC2 format:

def read_windsimu(path, file_name, Nx, Ny, Nz, opt='all', shear=False):
    """
    Reads Turbulence Box HAWC2 binary files, and returns
    u, v, w arrays.

    Parameters
    ----------
    path: str
        Folder where turbulence box is stored.
    file_name : str
        HAWC2 Turbulence box Binary File *.bin
    Nx, Ny, Nz: int
        Number of points in X, Y, Z direction.

    Returns
    -------
    u, v, w: array_like
        3D array with size (Nx, Ny, Nz) for each wind
        component.

    """
    # To check if the option is valid
    allowed_options = ['all', 'u']
    func.check_allow_opt(opt, allowed_options)
   
    if opt=='u':

        if shear:
            # To read file with added shear:
            u_file_path = path + file_name + '_u_shear.bin'
        else:  
            u_file_path = path + file_name + '_u.bin'

        # Read in the turb files
        with open(u_file_path, 'rb') as bin_fid:
            u = np.fromfile(
                bin_fid,
                dtype=MANN_BOX_FMT).reshape((Nx, Ny, Nz))
        return u
        
    else:
        if shear: 
            # To read file with added shear:
            u_file_path = path + file_name + '_u_shear.bin'
        else:
            u_file_path = path + file_name + '_u.bin'

        v_file_path = path + file_name + '_v.bin'
        w_file_path = path + file_name + '_w.bin'
        # Read in the turb files

        with open(u_file_path, 'rb') as bin_fid:
            u = np.fromfile(
                bin_fid,
                dtype=MANN_BOX_FMT).reshape((Nx, Ny, Nz)
                                            )
        with open(v_file_path, 'rb') as bin_fid:
            v = np.fromfile(
                bin_fid,
                dtype=MANN_BOX_FMT).reshape((Nx, Ny, Nz)
                                            )
        with open(w_file_path, 'rb') as bin_fid:
            w = np.fromfile(
                bin_fid,
                dtype=MANN_BOX_FMT).reshape((Nx, Ny, Nz)
                                            )
        return u, v, w


# %% Function to save u, v, w binary files HAWC2 format:


def df_to_h2turb(u, v, w, path, fname, u_shear=None):
    """
    Save u, v, w wind components from an array of shape
    (Nx, Ny, Nz) to a HAWC2 binary file with name:
    prefix_u.bin, prefix_v.bin, prefix_w.bin.

    Parameters
    ----------
    u, v, w: array_like
        3D array with size (Nx, Ny, Nz) for each wind
        component.
    path: str
        Folder where turbulence box is stored.
    fname : str
        Name of the HAWC2 binary file.
    u_shear: array_like, optional
        3D array with size (Nx, Ny, Nz) for the u component
        with shear profile added.
    
    Returns
    -------
    None

    """

    bin_path = os.path.join(path, f'{fname}_u.bin')
    print(bin_path)
    # assert os.path.isfile(bin_path)
    with open(bin_path, 'wb') as bin_fid:
        u.astype(np.dtype(MANN_BOX_FMT)).tofile(bin_fid)

    bin_path = os.path.join(path, f'{fname}_v.bin')
    print(bin_path)
    # assert os.path.isfile(bin_path)
    with open(bin_path, 'wb') as bin_fid:
        v.astype(np.dtype(MANN_BOX_FMT)).tofile(bin_fid)
            
    bin_path = os.path.join(path, f'{fname}_w.bin')
    print(bin_path)
    # assert os.path.isfile(bin_path)
    with open(bin_path, 'wb') as bin_fid:
        w.astype(np.dtype(MANN_BOX_FMT)).tofile(bin_fid)
            
    print('Turbulence box has been saved as HAWC2 '
          f'formnat as {bin_path}. \n')
    
    if u_shear is not None:
        bin_path = os.path.join(path, f'{fname}_u_shear.bin')
        print(bin_path)
        # assert os.path.isfile(bin_path)
        with open(bin_path, 'wb') as bin_fid:
            u_shear.astype(np.dtype(MANN_BOX_FMT)).tofile(bin_fid)

        print('Turbulence box has been saved as HAWC2 '
        f'formnat as {bin_path}. \n')

    return


# %% Function to extract turbulence box from NetCDF file:


def extract_turbbox_from_netcdf(path, case, comp='all'):
    """
    Extracts turbulence box from NetCDF file, and returns
    u, v, w arrays.

    Parameters
    ----------
    path: str
        Folder where turbulence box is stored.
    case : str
        Name of the NetCDF file.
    comp: str
        Component to be extracted, can be 'u', 'v', 'w' or
        'all'.
    
    Returns
    -------
    array: array_like
        3D array with size (Nx, Ny, Nz) for each wind
        component.

    """
    
    # To check if the option is valid
    allowed_options = ['u', 'v', 'w', 'u_shear', 'all']
    func.check_allow_opt(comp, allowed_options)
    
    file_path = path + case + '.nc'
    ds = xr.open_dataset(file_path, engine="netcdf4", group='mann_tbox')
    
    if comp in ('u', 'v', 'w', 'u_shear'):
        array = ds[comp].values
        return array
    
    else: 
        u_array = ds['u'].values
        v_array = ds['v'].values
        w_array = ds['w'].values
        u_shear_array = ds['u_shear'].values
        return u_array, v_array, w_array, u_shear_array
    
# %% Function to add the shear profile to the u component:


def add_shear_hawc2(u, wsp, z_hub, z_min, tb_ht, Nz, alpha=0.2,
                    shear='power_law'):

    """
    Add shear to turbulence box, having two options for
    profile: Constant, Power law, which is the default one,
    or none, which will keep the same u.

    Parameters
    ----------
    u: array
        u component for the wind speed, with array size
        (Nx, Ny, Nz).
    wsp: float
        Wind speed [m/s]
    z0: float
        Initial position on z coordinate [m].
    tb_ht: float
        Height of turbulence box [m].
    Nz: float
        Number of grids in the Z coordinate.
    alpha: float, optional
        Exponent for the power law, where alpha is an
        empirically derived coefficient that varies dependent
        upon the stability of the atmosphere (Typically
        assumed to be equal to 0.2).
    shear: str, optional
        Definition of the format to add the shear to the
        turbulence box. By default power_law is selected.

    Returns
    -------
    u_new: array
        u component for the wind speed, with array size same
        as u input, but now with the wind shear profile
        added to the u component.

    """
    # To check if the option is valid
    allowed_options = ['none', 'cte', 'power_law', 'linear']
    func.check_allow_opt(shear, allowed_options)
    
    def check_sign_difference(x, y):
        return np.sign(x) == np.sign(y)
    
    dudz = -0.0288  # From Wei Fu Paper
    Z = np.linspace(z_min, (z_min +  tb_ht), Nz)
    #Z = np.flip(Z, axis=None)

    # Initilize the 3D array, based on u shape:
    u_new = np.zeros(u.shape)
    # Select shear profile (Constant or Power Law method):
    print('Adding shear profile selected: ' + shear + ' \n')
    if shear == 'cte':
        # Add wind speed constant to the u array:
        u_new = u + wsp
    elif shear == 'power_law':
        i = 0
        for elem in Z:
            # Generates array to be added based on power law:
            # Checks the sign, to ensure that we add zero and no NaN if
            # the turbulence box goes below the floor.
            if check_sign_difference(elem, z_hub): 
                u_add = wsp * ((elem/z_hub)**alpha)
            #    Adds wind speed to u component:
                u_new[:, :, i] = u[:, :, i] + u_add
            elif elem == 0:
                u_new[:, :, i] = u[:, :, i]
            else: 
                u_new[:, :, i] = u[:, :, i] * 0  # Replace for zero under the ground.
            i = i + 1
        print('Wind Shear has been added. \n')

    elif shear == 'none':
        u_new = u
        
    elif shear == 'linear':  
        i = 0
        for elem in Z:

            if check_sign_difference(elem, z0): 
                u_add = wsp + dudz*(elem - z0)
            #    Adds wind speed to u component:
                u_new[:, :, i] = u[:, :, i] + u_add
            else: 
                u_new[:, :, i] = u[:, :, i] * 0
            i = i + 1
        
    else:
        raise ValueError('Shear method needs to be none, cte, linear',
                         ' or power_law.')
    return u_new


# %% Function to extract free wind from HAWC2 binary file:


def extract_fw_hawc2(path, fname, idx_init, idx_final, Nyz):
    """
    Extracts free wind from HAWC2 binary file, and returns
    a DataFrame with the time series for the free wind.
    This is to perform the comparison between the free wind
    and the turbulence box, to ensure that the turbulence box
    is read correctly, and the shear profile is added correctly.

    Parameters
    ----------
    path: str
        Folder where turbulence box is stored.
    fname : str
        Name of the HAWC2 binary file.
    idx_init, idx_final: int
        Initial and final index for the free wind.
    Nyz: int
        Number of points in the Y and Z direction.
    
    Returns
    -------
    data: DataFrame
        DataFrame with the time series for the free wind.
    """
    
    hawc2_path = path + fname + '.hdf5'
    read_h2 = ReadHawc2(hawc2_path)
    hawc2_res = read_h2.ReadGtsdf()

    # Time series:
    Time = hawc2_res[:, 0]
        
    # To extract free wind, based on test file:
    step = 3

    # Create X and Z arrays
    x1 = np.concatenate([np.zeros(Nyz), np.linspace(-200, 200, Nyz)])
    x2 = np.concatenate([np.zeros(10), np.linspace(-200, 200, 10)])
    X = np.concatenate([x1, x2])
    z1 = np.concatenate([np.linspace(0, -400, Nyz), np.full(Nyz, -200)])
    z2 = np.concatenate([np.linspace(0, -400, 10), np.full(10, -119)])
    Z = np.concatenate([z1, z2])

    # Create empty lists to store the data
    Time_data = []
    Vx_data = []
    Vy_data = []
    Vz_data = []
    X_data = []
    Z_data = []
    Vx_da = []
    Vy_da = []
    Vz_da = []

    indices = np.arange(idx_init, idx_final, step)

    for idx in indices:
        Vx_data.append(hawc2_res[:, idx])
        Vy_data.append(hawc2_res[:, idx+1])
        Vz_data.append(hawc2_res[:, idx+2])

    for i in range((Nyz + 10)*2):
        Vx_da.extend(Vx_data[i].flatten())
        Vy_da.extend(Vy_data[i].flatten())
        Vz_da.extend(Vz_data[i].flatten())
        Time_data.extend(Time.flatten()) # Repeat Time for each element in Vx_data[i]
        X_data.extend((np.zeros(Time.shape) + X[i]).flatten())
        Z_data.extend((np.zeros(Time.shape) + Z[i]).flatten())

    data = pd.DataFrame({
        'Time': Time_data,
        'Vx_data': Vx_da,
        'Vy_data': Vy_da,
        'Vz_data': Vz_da,
        'Xg': X_data,
        'Zg': Z_data
    })
    
    return data


# %% Turbulence Box Class:


class TurbBox:

    """
    Turbulence Box class that containst all the parameters
    required for a turbulence box generation.

    Attributes
    ----------
    wsp: float
        Wind Speed [m/s].
    ae_: float
        Alpha (kolmogorov's spectral constant) epsilon
       (energy disipation) to the power of 2/3 [m**4/3*s**-2]
       (the default is None, in which case takes the value
        of alphaepsilon in the dictionary kw.)
    folder_: str
        Folder to read HAWC2 binary file turbulence box,
        and to save the u component binary file after shear
        profile is added.
    prefix_: str
        File name for the turbulence boxes, based on case
        scenarios.
   shear: str, optional
       Definition of the format to add the shear to the
       turbulence box. By default power_law is selected.
   arg: dict
       Dictionary with information for the Turbulence box
       generation.

    Methods
    -------
    df_to_h2turb()
        Save u wind component from an array of shape
        (Nx, Ny, Nz) to a HAWC2 binary file with name:
        prefix_u.bin.

    """

    def __init__(self, folder_, prefix_, shear_, opt, **arg):

        # To check if the option is valid
        allowed_options = ['hawc2', 'netcdf']
        func.check_allow_opt(opt, allowed_options)

        self.wsp = fNet.extract_wsp_text(prefix_) # Wind speed [m/s]
        self.alphaepsilon = fNet.extract_ae_text(prefix_)  # Mann Model parameter.
        
        # Definition of parameters for the turbulence box:
        self.Nx = arg['Nx']  # Number of points Yg direction.
        self.Ny = arg['Ny']  # Number of points Xg direction.
        self.Nz = arg['Nz']  # Number of points Zg direction.
        
        self.Time = arg['Time']
        
        self.Ly = self.wsp * self.Time
        self.Lx = arg['Ly']  # Width turbulence box.
        self.Lz = arg['Lz']  # Height turbulence box.
        
        self.tb_dy = ((self.wsp * arg['Time']) / arg['Nx'])
        self.tb_dx = arg['tb_dy']  # Space grid in Xg
        self.tb_dz = arg['tb_dz']  # Space grid in Zg
        
        self.y0 = arg['hub_pos_y0']  # y0 HAWC2 Coordinate system.
        self.x0 = arg['hub_pos_x0']  # x0 HAWC2 Coordinate system.
        self.z0 = arg['hub_pos_z0']  # z0 HAWC2 Coordinate system.
        self.z_min = 0  # Lowest height of tbox in glb. coord. system.
        
        self.alpha = arg['alpha']  # Exponent Shear profile.
        #self.folder = PROJECT_ROOT + '/' + folder_
        self.folder = folder_ # FIXME
        self.prefix = prefix_  # Name turbulence box.
        self.shear = shear_  # Shear profile selected.

        # Creating coordinate system in Xg, Yg, Zg, t for turbulence box:
        self.Yg = np.linspace(0, self.Ly, self.Nx)
        self.Xg = np.linspace((self.Lx/2), -(self.Lx/2), self.Ny)
        self.Zg = -np.linspace(0, self.Lz, self.Nz)  # To match HAWC2 coord. system.
        self.t = np.linspace(0, self.Time, self.Nx)
        
        # Reading HAWC2 format Binary files for
        # turbulence boxes:
        if opt == 'hawc2':
            self.u_raw, self.v_raw, self.w_raw = read_windsimu(
                    path=self.folder,
                    file_name=self.prefix,
                    Nx=self.Nx,  # Longitudinal direction
                    Ny=self.Ny,  # Lateral direction
                    Nz=self.Nz   # Vertical direction
                    )
        elif opt == 'netcdf':  # Read turbulence box from NetCDF file:
            self.u_raw, self.v_raw, self.w_raw = extract_turbbox_from_netcdf(
                                                    path=self.folder,
                                                    case=self.prefix,
                                                    comp='all')
        
        # Adding shear profile to u component:
        self.u = add_shear_hawc2(self.u_raw,
                                 self.wsp,
                                 abs(self.z0),
                                 self.z_min,
                                 self.Lz,
                                 self.Nz,
                                 self.alpha,
                                 self.shear)
        
        # Create meshgrid for organizing DataFrame TurbBox:
        idxx2d, idxz2d = np.meshgrid(np.arange(self.Ny), np.arange(self.Nz))
        x2d, z2d = np.meshgrid(self.Xg, self.Zg)
        x2d = x2d.T
        z2d = z2d.T
        y3d = np.zeros((self.Nx, self.Ny, self.Nz))
        x3d = np.tile(x2d, (self.Nx, 1, 1))
        z3d = np.tile(z2d, (self.Nx, 1, 1))
        idxx3d = np.tile(idxx2d, (self.Nx, 1, 1))
        idxz3d = np.tile(idxz2d, (self.Nx, 1, 1))
        idxy3d = np.zeros((self.Nx, self.Ny, self.Nz))
        t3d = np.zeros((self.Nx, self.Ny, self.Nz))
        idxt3d = np.zeros((self.Nx, self.Ny, self.Nz))
        
        for k, val in enumerate(self.t):
            t3d[k, :, :] = val
            idxt3d[k, :, :] = k
        for k, val in enumerate(self.Yg):
            x3d[k, :, :] = val
            idxx3d[k, :, :] = k

        res = np.hstack((
                 idxx3d.flatten()[:, None],
                 idxy3d.flatten()[:, None],
                 idxz3d.flatten()[:, None],
                 idxt3d.flatten()[:, None],
                 y3d.flatten()[:, None],
                 x3d.flatten()[:, None],
                 z3d.flatten()[:, None],
                 t3d.flatten()[:, None],
                 self.u.flatten()[:, None],
                 self.u_raw.flatten()[:, None],
                 self.v_raw.flatten()[:, None],
                 self.w_raw.flatten()[:, None]))
        del (x2d, z2d, x3d, y3d, z3d, t3d, idxx2d, idxz2d,
             idxy3d, idxz3d, idxt3d, idxx3d)
        self.as_df = pd.DataFrame(
            data=res, columns=[
                'idx_x', 'idx_y', 'idx_z', 'idx_t',
                'Xg', 'Yg', 'Zg', 't', 'u',
                'u_raw', 'v_raw', 'w_raw'
                ])
        self.as_df[[
            'idx_x', 'idx_y', 'idx_z', 'idx_t'
            ]] = self.as_df[['idx_x', 'idx_y', 'idx_z',
                             'idx_t']].astype(int)
        # Calculates Total Wind Speed [m/s]:
        self.as_df['U_tot'] = np.sqrt((self.as_df.u)**2 +
                                      (self.as_df.v_raw)**2 +
                                      (self.as_df.w_raw)**2)
        # Set index for DataFrame:
        self.as_df.set_index(['idx_x', 'idx_y', 'idx_z',
                              'idx_t'], inplace=True)
        del res


    def df_to_h2turb_shear(self):

        """
        Function to save one wind component from an array
        of shape (Nx, Ny, Nz) to a HAWC2 binary file
        prefix_u.bin

        This is use since we modified the u component to
        add the shear, and later, we will modified the
        components to add the induction.

        It will overwrite the original u file of the
        turbulence box.

        """

        # make and save binary files for one component.
        for c in 'u':
            bin_path = os.path.join(self.folder,
                                    f'{self.prefix}_{c}_shear.bin')
            print(bin_path)
            # assert os.path.isfile(bin_path)
            with open(bin_path, 'wb') as bin_fid:
                self.u.astype(np.dtype(MANN_BOX_FMT)).tofile(bin_fid)
        print('Turbulence box has been saved as HAWC2 '
              f'formnat as {bin_path}. \n')
        return


    def plot_tb2d(self):
        """
        Generates a Figure with the three wind components
        u, v, w in the YZ plane.

        Returns
        -------
        fig : matplotlib.figure
            Figures with the u, v, w components.

        """
        # Calculate 2d plane average:
        u_2dmean = np.mean(self.u, axis=0)
        v_2dmean = np.mean(self.v_raw, axis=0)
        w_2dmean = np.mean(self.w_raw, axis=0)
        # calculations for csv plot
        ycsv_idx = ((self.Xg >= self.Xg.min()) &
                    (self.Xg <= self.Xg.max()))
        zcsv_idx = ((self.Zg >= self.Zg.min()) &
                    (self.Zg <= self.Zg.max()))
        ims_ext = [self.Xg[ycsv_idx].min(),
                   self.Xg[ycsv_idx].max(),
                   self.Zg[zcsv_idx].max(),
                   self.Zg[zcsv_idx].min()]

        # Plot Figures for u component:
        fig, axes = plt.subplots(nrows=1,
                                 ncols=3,
                                 figsize=(19, 4.5))
        fig.subplots_adjust(wspace=0.3)
        
        img0 = axes[0].imshow(
            u_2dmean[ycsv_idx][:, zcsv_idx].T,
            interpolation='spline16',
            origin='lower',
            extent=ims_ext,
            )
        axes[0].set_title('u component', fontweight='bold')
        axes[0].set_xlabel('Xg [m]', fontweight='bold')
        axes[0].set_ylabel('Zg [m]', fontweight='bold')
        axes[0].axis('tight')
        cbar1 = fig.colorbar(img0, ax=axes[0], shrink=1, pad=0.02)
        cbar1.ax.yaxis.set_ticks_position('right')
        cbar1.set_label('Wind speed u [m/s]', fontsize= 12, fontweight='bold', labelpad=10)

        img1 = axes[1].imshow(
            v_2dmean[ycsv_idx][:, zcsv_idx].T,
            interpolation='spline16',
            origin='lower',
            extent=ims_ext
            )
        axes[1].set_title('v component', fontweight='bold')
        axes[1].set_xlabel('Xg [m]', fontweight='bold')
        axes[1].axis('tight')
        cbar2 = fig.colorbar(img1, ax=axes[1], shrink=1, pad=0.02)
        cbar2.ax.yaxis.set_ticks_position('right')
        cbar2.set_label('Wind speed v [m/s]', fontsize= 12, fontweight='bold', labelpad=10)

        img2 = axes[2].imshow(
            w_2dmean[ycsv_idx][:, zcsv_idx].T,
            interpolation='spline16',
            origin='lower',
            extent=ims_ext
            )
        axes[2].set_title('w component', fontweight='bold')
        axes[2].set_xlabel('Xg [m]', fontweight='bold')
        axes[2].axis('tight')
        cbar3 = fig.colorbar(img2, ax=axes[2], shrink=1, pad=0.02)
        cbar3.ax.yaxis.set_ticks_position('right')
        cbar3.set_label('Wind speed w [m/s]', fontsize= 12, fontweight='bold', labelpad=10)

        fig.suptitle('Average wind speed component across plane Xg-Zg', fontsize=14, fontweight='bold')

        return fig


    def calc_1D_stats(self):
        # Mean wind speed:
        u_mean = np.mean(self.u)
        v_mean = np.mean(self.v_raw)
        w_mean = np.mean(self.w_raw)
        # Variance in YZ plane:
        u_var = np.var(self.u)
        v_var = np.var(self.v_raw)
        w_var = np.var(self.w_raw)
        # Standard Deviation:
        u_std = np.std(self.u)
        v_std = np.std(self.v_raw)
        w_std = np.std(self.w_raw)
        # Mnimum:
        u_min = np.min(self.u)
        v_min = np.min(self.v_raw)
        w_min = np.min(self.w_raw)
        # Maximum:
        u_max = np.max(self.u)
        v_max = np.max(self.v_raw)
        w_max = np.max(self.w_raw)

        # Data:
        data = {'u_mean': u_mean,
                'v_mean': v_mean,
                'w_mean': w_mean,
                'u_var': u_var,
                'v_var': v_var,
                'w_var': w_var,
                'u_std': u_std,
                'v_std': v_std,
                'w_std': w_std,
                'u_min': u_min,
                'v_min': v_min,
                'w_min': w_min,
                'u_max': u_max,
                'v_max': v_max,
                'w_max': w_max
                }
        del (u_mean, v_mean, w_mean,
             u_var, v_var, w_var,
             u_std, v_std, w_std,
             u_min, v_min, w_min,
             u_max, v_max, w_max)
        return data
