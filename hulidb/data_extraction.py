
'''
@author: espa

Functions to extract the data from the NETcdf file, and compare it with the HAWC2 results.
Functions also to compare and test if the data has been saved correctly.
'''
import os 
import time
import pickle
import numpy as np
import pandas as pd
import xarray as xr
from netCDF4 import Dataset
import hulidb.functions as func
import hulidb.turbbox as tbox


# %% Function to find the matching beams index:


def find_matching_beams(df, lidar_config):
    """
    Find the beams number on the file, to match the configuration required
    from the user. 

    Parameters
    ----------
    df : TYPE
        DESCRIPTION.
    lidar_config : TYPE
        DESCRIPTION.

    Returns
    -------
    matching_beams : dict
        Contain the dictionary, for each lidar configuration selected, with the
        beam number output in HAWC2 that will match the required combination
        configuration selected. 
        Example: If a configuration of six beam lidar has been requested, it
        will generate a dict for each beam, with key b1, b2, ..., b6,
        providing the index of each beam based on the azimuthal and half-cone
        angle, and the focus lenght. 
        This dict is used later, to extract the data from the NetCDF file.

    """
    
    matching_beams = {}
          
    for config_index, config in enumerate(lidar_config):
    
        assert len(config['theta']) == len(config['psi']), f"The number of theta and psi angles must be the same for config {config_index}."
        assert all(isinstance(x, (int, float, np.integer, np.floating)) for x in config['theta']), "All theta angle values must be int or float."
        assert all(isinstance(x, (int, float, np.integer, np.floating)) for x in config['psi']), "All psi angle values must be int or float."
        assert all(isinstance(x, (int, float, np.integer, np.floating)) for x in config['Focus-Length']), "All Focus-Length values must be int, float, or NumPy numeric types."

        matching_beams[config_index] = {}          
        
        # Iterate over each combination of theta and psi
        for i in range(len(config['theta'])):
            theta = config['theta'][i]
            psi = config['psi'][i]
            focus_lengths = config['Focus-Length']
            matching_beams[config_index][f'b{i+1}'] = {}
            
            # Filter further based on current theta and psi
            temp_df = df[(df['theta'] == theta) & (df['psi'] == psi)]
            
            # Find all matching Beam Nos for the current theta and psi across all focus lengths
            matches = temp_df[temp_df['Focus-Length'].isin(focus_lengths)]['Beam No'].tolist()
                        
            # Store the matching Beam Nos using a tuple key for theta and psi
            matching_beams[config_index][f'b{i+1}']['idx'] = matches
            matching_beams[config_index][f'b{i+1}']['psi'] = psi
            matching_beams[config_index][f'b{i+1}']['theta'] = theta
            matching_beams[config_index][f'b{i+1}']['FL'] = focus_lengths
                        
            # Message to ensure all configurtion are selected:
            for key in matching_beams[config_index]:
                
                if len(matching_beams[config_index][key]) != len(focus_lengths):
                    print(f'Configuration {config_index}, Beam {key} does not '
                          'have all selected configurations.\nCheck that '
                          'theta, psi and length ranges are among the values '
                          'selected for the Database.\n')
                
    return matching_beams


# %% Re-arrange the dictionary from HAWC2 measurement test case to be compared:



def rearrange_lidar_mes(Lidar_Measurements, time_str):
    """
    It rearrange the dictionary Lidar_Measurements_HAWC2 into the final
    dictionary with the DataFrames per configuration, as we want to for the
    final evaluation.

    Parameters
    ----------
    Lidar_Measurements_HAWC2 : dict
        Dictionary with with all the lidar configurations per inflow condition,
        with each beam data. 
    time_str: str
        It is the name of the time column in the dictionary. For HAWC2 results
        is 't_hawc2', and for the NETcdf case is 'Time'.

    Returns
    -------
    DF_lidar_HAWC2 : dict
        Dictionary with all the beam data combine under the specific lidar
        configuration.

    """

    # Organize and rearrange based on 't_hawc2' column:
    DF_lidar = {}

    for key, arr in Lidar_Measurements.items():
        DF_lidar[key] = {}
        for i in range(len(arr)):
            DF_lidar[key][i] = pd.concat(arr[i].values(), axis=0)
            DF_lidar[key][i] = DF_lidar[key][i].sort_values(by=time_str)
            DF_lidar[key][i] = DF_lidar[key][i].reset_index(drop=True)
            
    return DF_lidar


# %% 


def sort_nested_dataframes(data_dict):
    """
    Sorts all DataFrames in a nested dictionary structure by specified columns
    ('t_hawc2', 'Beam No') and the absolute value of 'y_hawc2' in ascending
    order.

    Parameters:
    data_dict (dict): The nested dictionary containing DataFrames.

    Returns:
    dict: The dictionary with sorted DataFrames.
    """
    

    for case_key, inner_dict in data_dict.items():
        # Check if the value is a dictionary (which contains DataFrames)
        if isinstance(inner_dict, dict):
            # Iterate over the inner dictionary by key (dataframe indices)
            for df_key, df in inner_dict.items():                    
                df_copy = df.copy()
                df_copy['abs_y_hawc2'] = df_copy['y_hawc2'].abs()
                df_copy = df_copy.sort_values(by=['t_hawc2', 'Beam No', 'abs_y_hawc2'], ascending=[True, True, True])
                df_copy.drop('abs_y_hawc2', axis=1, inplace=True)
                data_dict[case_key][df_key] = df_copy
    
    return data_dict
    

# %% Function to evaluate the new dict with old version of dict:


def eval_DF_lidar_dict(DF_lidar_HAWC2, DF_lidar_NETcdf, Cases):
    """
    Internal function to compare the generation of the dictionary with the data,
    directly from the result HAWC2 file, and extracting the data from the 
    NETcdf file. The idea is to compare if the values extracted and the 
    sampling frequency are correct. 
    If any difference is found, then an issue has arise. 
    
    Note: This function will not be part of the official repo! it is just
    for testing internally.

    Parameters
    ----------
    DF_lidar_HAWC2 : dict
        Dictionary with the lidar configuration data for HuLi based on the 
        previous version of the implementation, reading values from res file
        from HAWC2.
        
    DF_lidar_NETcdf : dict
        Dictionary with the lidar configuration data for HuLi based on the new
        implementation, reading the values from the NETcdf file.

    Returns
    -------
    Assertion error if one of the columns does not match at each time step. 
    """
    column_mapping = {
        'Time': 't_hawc2',
        'shaft_rot_angle': 'omega',
        'HuLi_Xg': 'x_hawc2',
        'HuLi_Yg': 'y_hawc2',
        'HuLi_Zg': 'z_hawc2',
        'HuLi_V_LOS_wgh': 'LOS_wsp',
        'HuLi_V_LOS_nom': 'Nom_wsp',
    }

    try:
        for case in Cases:
            for i in range(5):
                for new_col, orig_col in column_mapping.items():
                    new_values = DF_lidar_NETcdf[case][i][new_col].to_numpy()
                    orig_values = DF_lidar_HAWC2[case][i][orig_col].to_numpy()
                    
                    # Check if lengths match
                    assert len(new_values) == len(orig_values), f"Length mismatch for {new_col} in case {case} iteration {i}"
                    # Check if values are close enough
                    assert np.allclose(new_values, orig_values, atol=1e-6), f"Value mismatch for {new_col} in case {case} iteration {i}"
        return "All values match successfully."
    
    except AssertionError as e:
        return str(e) 
    

# %%


def load_data_for_time_beam(nc_file_path, idx_time, beams, group):
    """
    Loads the data from the NETcdf file specific group, into a DataFrame, from
    where the data is stored in the dictionaries. 

    Parameters
    ----------
    nc_file_path : str
        NETcdf file name, with the extension .nc
    idx_time : array
        array that contains the beam number to which we need to extract the data.
        Noticed that due to Python index from 0, we need to substract one from
        the original generated beam number from HAWC2 definition. 
    beams : dict
        Dictionary that contains the information for each configuration
        required, also stored inside a dictionary. 
        Each dictionary, contain the information for each beam: Range (FL), 
        azimuthal angle (psi), half-cone angle (theta), and the index of the
        beam in HAWC2 (idx + 1).
        Noticed that the reason why for one beam, multiple beams in HAWC2 are
        required, is to capture the multiple ranges. 
    group : str
        Name of the group from where we need to read the data. It can be:
            HuLi_data: For hub-lidar sensor data channels.
            wt_res: For aeroelatic turbine response channels. 

    Returns
    -------
    df : DataFrame
        Returns the desire data from the NETcdf file to be append in the final
        DataFrame. 
        It extracts the data based on the index time and the beam number, which
        are dimensions on the NETcdf file. 

    """
    
    file_path = './NETcdf/' + nc_file_path
    
    if group == 'HuLi_data':
        ds = xr.open_dataset(file_path, engine="netcdf4", group=group)
        ds_selected = ds.isel(time=idx_time, beam_num=beams)
        df = ds_selected.to_dataframe()
        
        ds.close()
            
    elif group == 'wt_res':
        ds = xr.open_dataset(file_path, engine="netcdf4", group=group)
        ds_selected = ds.isel(time=idx_time)[beams]
        df = ds_selected.to_dataframe()
        
        ds.close()
            
    else:
        print(f'Error: Selected group is not valid.')
        
    return df


#%%


def extract_HuLi_data_from_NETcdf(
        file_path, beams, sampling_freq, len_th):
    """
    It reads the NETcdf file with all the results, and extract the desire
    hub-lidar configurations based on 'beams' dictionary and the sampling
    frequency. 
    The result will be a dictionary for one inflow case, with all the desire
    configurations. Each configuration data is stored as a DataFrame.

    Parameters
    ----------
    file_path : str
        Name of the case, which will be the same of the NETcdf file, withou
        the extension '.nc'.
    beams : dict
        Dictionary that contains the information for each configuration
        required, also stored inside a dictionary. 
        Each dictionary, contain the information for each beam: Range (FL), 
        azimuthal angle (psi), half-cone angle (theta), and the index of the
        beam in HAWC2 (idx + 1).
        Noticed that the reason why for one beam, multiple beams in HAWC2 are
        required, is to capture the multiple ranges. 
    sampling_freq : float
        Sampling frequency in Hz for each individual beam on the lidar configuration. 
    len_th : int
        Size or length of the time array inside the data. In this case, the
        value is 12,000, equivalent to 10-minute simulation, with deltat=0.05 s.  

    Returns
    -------
    DF_lidar : dict
        Contains all the inflow cases dictionaries.
        Each of these dictionaries, contain multiple DataFrames equal to the
        number of configurations desire.
        Each DataFrame will have the information from the HuLi sensor. 

    """
       
    t_scan = 1 / sampling_freq  # Time step based on sampling frequency
    columns = ['shaft_rot_angle', 'hub1_pos_x', 'hub1_pos_y', 'hub1_pos_z']
        
    DF_lidar = {}    
    nc_file_path = file_path + '.nc'
    
    accumulative_time = 0
    
    # Extraction of data based on each configuration desire: 
    for config_key, beams_info in beams.items():
        
        beam_time_offset = {
            beam_key: round(i * t_scan, 2) for i,
            beam_key in enumerate(beams_info.keys())
            }

        all_dataframes = []
        
        # Looking for each beam in the selected configuration:
        for beam_key, beam_data in beams_info.items():
            
            # To measure how long it takes to compute the data:
            start = time.time()
            
            # To define the time step for each beam: 
            start_time = beam_time_offset[beam_key] + 0.05
            start_index = int(np.round(start_time / 0.05) - 1)
            step_size = int(t_scan * len(beams_info) / 0.05)
            idx_time = np.arange(start_index, len_th, step_size).astype(int)

            print(f'Extracting data for configuration {config_key+1}, beam: {beam_key}')
               
            #beam_batch = beam_data['idx']
            beam_batch = [x - 1 for x in beam_data['idx']]
            
            df_batch = load_data_for_time_beam(nc_file_path, 
                                               idx_time, 
                                               beam_batch, 
                                               group='HuLi_data')
            
            # To modify 'time' from index to column:
            df_batch.reset_index('time', inplace=True)
            df_batch.rename(columns={'time': 'time_beam'}, inplace=True)
                   
            df_wt = load_data_for_time_beam(nc_file_path, 
                                               idx_time, 
                                               columns, 
                                               group='wt_res')
            
            # To modify 'time' from index to column:
            df_wt.reset_index('time', inplace=True)
            df_wt.rename(columns={'time': 'time_wt'}, inplace=True)
                   
            # Add additional columns to df_batch
            df_batch['Theta'] = beam_data['theta']
            df_batch['Psi'] = beam_data['psi']
            df_batch['Beam_No'] = beam_key
                           
            merged_df = pd.merge(
                df_batch, df_wt, left_on='time_beam', 
                right_on='time_wt', how='left'
                )
            
            # to drop and rename time column:
            merged_df.drop(columns=['time_wt'], inplace=True)
            merged_df.rename(columns={'time_beam': 'Time'}, inplace=True)
            merged_df['Time'] = merged_df['Time'].dt.total_seconds()
            
            all_dataframes.append(merged_df)
            
            # To evaluate time elapse during extraction:
            end = time.time()
            interval = end - start
            accumulative_time = accumulative_time + interval

        print(f'Total time extraction: {accumulative_time:.3f} seconds.\n')

        # Assuming all_dataframes is the list containing your batch DataFrames
        final_df = pd.concat(all_dataframes)
    
        # Calculate absolute Yg for sorting
        final_df['abs_Yg'] = final_df['HuLi_Yg'].abs()
    
        # Sort the final DataFrame
        final_df.sort_values(by=['Time', 'abs_Yg', 'Beam_No'], ascending=[True, True, True], inplace=True)
    
        # Optional: Drop the abs_Yg if no longer needed after sorting
        final_df.drop(columns='abs_Yg', inplace=True)
    
        new_order = [
            'Time', 'shaft_rot_angle', 'hub1_pos_x', 'hub1_pos_y', 'hub1_pos_z',
            'HuLi_Xg', 'HuLi_Yg', 'HuLi_Zg', 'HuLi_V_LOS_wgh', 'HuLi_V_LOS_nom',
             'Theta', 'Psi', 'Beam_No'
        ]
    
        final_df = final_df[new_order]
        
        config_name = 'config_' + str(config_key + 1)                    
        DF_lidar[config_name] = final_df
        
    return DF_lidar


# %% Extract hub-lidar data for multiple inflow cases:

def extract_huli_data(Cases, df_lidar, lidar_config, sampling_freq,
                      t_start, t_end, deltat, 
                      path = None, fname = None, save=False):
    """
    Extract the data from the NETcdf file for multiple inflow cases, and
    multiple configurations. It will generate a dictionary with all the
    cases, and saved the dictionary in a pickle file, if desired.

    Parameters
    ----------
    Cases : list
        List with all the inflow cases to be extracted.
    df_lidar : DataFrame
        DataFrame with the lidar configuration data for HuLi.
    lidar_config : list
        List of dictionaries with the lidar configurations, with the 
        azimuthal angle, half-cone angle, and the focus length.
    sampling_freq : float
        Sampling frequency in Hz for each individual beam on the
        lidar configuration.
    t_start : float
        Start time for the extraction of the data.
    t_end : float
        End time for the extraction of the data.
    deltat : float
        Time step for the extraction of the data.
    path : str, optional
        Path where the DataFrame will be saved. The default is None.
    fname : str, optional
        Name of the file to save the DataFrame. The default is None.
    save : bool, optional
        If True, the DataFrame will be saved in a pickle file.
        The default is False.
    
    Returns
    -------
    DF_lidar_NETcdf : dict
        Dictionary with all the inflow cases dictionaries.
        Each of these dictionaries, contain multiple DataFrames equal to the
        number of configurations desire.
        Each DataFrame will have the information from the HuLi sensor, where
        you will have:
            - Time : Time in seconds for each syntethic data point.
            - shaft_rot_angle : Rotational angle of the blade 1 body in HAWC2
              global coordinate system. It helps to know the azimuthal angle of
              the beams, which will be equal to (shaft_rot_angle + psi).
            - hub1_pos_x : Position of the hub in the x-axis.
            - hub1_pos_y : Position of the hub in the y-axis.
            - hub1_pos_z : Position of the hub in the z-axis.
            - HuLi_Xg : Location in Xg of the hub-lidar measurement point.
            - HuLi_Yg : Location in Yg of the hub-lidar measurement point.
            - HuLi_Zg : Location in Zg of the hub-lidar measurement point.  
            - HuLi_V_LOS_wgh : Weighted line-of-sight wind speed.
            - HuLi_V_LOS_nom : Nominal line-of-sight wind speed.
            - Theta : Half-cone angle of the beam.
            - Psi: Azimuthal angle of the beam.
            - Beam_No : Beam tag on the selected configuration, as b1, b2, ..., b6.

    """

    # Calculating the index beam number in HAWC2:
    matching_beams = find_matching_beams(df_lidar, lidar_config) 
    time = np.arange(t_start, t_end, deltat)
    
    DF_lidar_NETcdf = {}
    for case in Cases:
       
        DF_lidar_NETcdf[case] = extract_HuLi_data_from_NETcdf(
            case, matching_beams, sampling_freq, time.size)
        
    if save: 
        # Ensure path exist, if not create the path:
        func.ensure_path_exists(path)
        
        file_path = path + fname + '.pkl'
        with open(file_path, 'wb') as file:
            pickle.dump(DF_lidar_NETcdf, file)    
        print(f'DataFrame has been saved in {file_path}.')
    
    return DF_lidar_NETcdf


# %% Extract aeroelastic response data for multiple inflow cases:


def extract_aero_response_from_NETcdf(
        path, file_name, path_to_save=None, fname=None, save=False
        ):
    """
    Extract the aeroelastic response data from the NETcdf file. It will generate
    a DataFrame with the data extracted, and save it in a pickle file, if desired.

    Parameters
    ----------
    path : str
        Path where the NETcdf file is stored.
    file_name : str
        Name of the NETcdf file, without the extension '.nc'.
    path_to_save : str, optional
        Path where the DataFrame will be saved. The default is None.
    fname : str, optional
        Name of the file to save the DataFrame. The default is None.
    save : bool, optional
        If True, the DataFrame will be saved in a pickle file.
        The default is False.
    
    Returns
    -------
    df : DataFrame
        DataFrame with the aeroelastic response data extracted from the NETcdf file.
    """
    
    file_path = path + file_name + '.nc'
    
    ds = xr.open_dataset(file_path, engine="netcdf4", group='wt_res')
    df = ds.to_dataframe()
    df.reset_index(inplace=True)
    df = df.drop(columns=['time', 'output_channel'])

    ds.close()
    
    if save: 
        # Ensure path exist, if not create the path:
        func.ensure_path_exists(path)
        
        file_path = path_to_save + fname + '.pkl'
        with open(file_path, 'wb') as file:
            pickle.dump(df, file)    
        print(f'DataFrame has been saved in {file_path}.')

    return df


# %% Load the dictionary from a pickle file:


def load_dictionary(path, file_name):
    """
    Load a dictionary from a pickle file.

    Parameters
    ----------
    path : str
        Path where the pickle file is stored.
    file_name : str
        Name of the pickle file, without the extension '.pkl'.
    
    Returns
    -------
    loaded_dict : dict
        Dictionary loaded from the pickle file.
    """
    
    file_to_open = path + file_name + '.pkl'
    with open(file_to_open, 'rb') as file:
        loaded_dict = pickle.load(file)
        
    return loaded_dict


# %% Load turbulence box u, v, and w from NetCDF file:


def extract_tbox_from_netcdf(
        path, file_name, path_to_save=None, fname=None, save=False
        ):
    
    file_path = path + file_name + '.nc'
    
    ds = xr.open_dataset(file_path, engine="netcdf4", group='turb_box')
    df = ds.to_dataframe()
    #df.reset_index(inplace=True)

    ds.close()

    if save:
        # Ensure path exist, if not create the path:
        func.ensure_path_exists(path)
        
        file_path = path_to_save + fname + '.pkl'
        with open(file_path, 'wb') as file:
            pickle.dump(df, file)    
        print(f'DataFrame has been saved in {file_path}.')

    return df


# %%

def extract_aero_resp(Cases, path, path_to_save=None, fname=None, save=False):
    """
    Extract the aeroelastic response data from the NETcdf file for multiple inflow cases.
    It will generate a dictionary with all the cases, and save the dictionary in a pickle
    file, if desired.

    Parameters
    ----------
    Cases : list
        List with all the inflow cases to be extracted.
    path : str
        Path where the NETcdf file is stored.
    path_to_save : str, optional
        Path where the DataFrame will be saved. The default is None.
    fname : str, optional
        Name of the file to save the DataFrame. The default is None.
    save : bool, optional
        If True, the DataFrame will be saved in a pickle file.
        The default is False.

    """

    DF_aero_NETcdf = {}
    for case in Cases:
        DF_aero_NETcdf[case] = extract_aero_response_from_NETcdf(
            path, case)
        
    if save: 
        # Ensure path exist, if not create the path:
        func.ensure_path_exists(path_to_save)
        
        file_path = path_to_save + fname + '.pkl'
        with open(file_path, 'wb') as file:
            pickle.dump(DF_aero_NETcdf, file)    
        print(f'DataFrame has been saved in {file_path}.')
    
    return DF_aero_NETcdf  


# %% Function to extract turbulnece box from NETcdf file:

def extract_tbox_from_netcdf(case, path_netcdf, path_save, hawc2_file=True):
    """
    Extract the turbulence box from the NETcdf file, and save it in a binary file
    for HAWC2. It will extract the turbulence box for the u, v, w, and u shear as HAWC2 will read it,
    and in the coordinate system of the Mann turbulence box. 

    Parameters
    ----------
    case : str
        Name of the case, which will be the same of the NETcdf file, without the
        extension '.nc'.
    path_netcdf : str
        Path where the NETcdf file is stored.
    path_save : str
        Path where the turbulence box will be saved.
    hawc2_file : bool, optional
        If True, the turbulence box will be saved in a binary file for HAWC2.
        The default is True.
    
    Returns
    -------
    u_turb : array
        Turbulence box for the u component.
    v_turb : array
        Turbulence box for the v component.
    w_turb : array
        Turbulence box for the w component.
    u_shear_turb : array
        Turbulence box for the u shear component.
    
    """
    
    file_path = path_netcdf + case + '.nc'
    
    # Open the NetCDF file
    with Dataset(file_path, 'r') as nc:
        # Access the group 'HuLi_data'
        Mann_data = nc.groups['mann_tbox']
    
        # Extract the variable 'time'
        u = Mann_data.variables['u'][:]
        u_turb = u[:, :, :].data

        v = Mann_data.variables['v'][:]
        v_turb = v[:, :, :].data

        w = Mann_data.variables['w'][:]
        w_turb = w[:, :, :].data
        
        u_shear = Mann_data.variables['u_shear'][:]
        u_shear_turb = u_shear[:, :, :].data
        
    if hawc2_file: 
    
        # To ensure the folder exists:
        if not os.path.exists(path_save):
            os.makedirs(path_save)
            print(f"Folder created: {path_save}")
        else:
            print(f"Folder already exists: {path_save}")
    
        # Saving the turbulence box into binary files:
        tbox.df_to_h2turb(u_turb, v_turb, w_turb, path_save, case, u_shear=u_shear_turb)

        return print(f'Turbulence box has been saved in {path_save}.')
    
    else: 
        return u_turb, v_turb, w_turb, u_shear_turb