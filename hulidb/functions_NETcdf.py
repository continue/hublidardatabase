# -*- coding: utf-8 -*-
"""
@author: espa

Function for the generation of the NETcdf file, extracting the data
from HAWC2 simulations.
"""

import time
import os
import re
import numpy as np
import pandas as pd
import xarray as xr
import netCDF4 as nc
from netCDF4 import Dataset
import hulidb.turbbox as tbox
import hulidb.functions as func
from wetb.hawc2.Hawc2io import ReadHawc2
from wetb.wind.turbulence.mann_parameters import var2ae
import matplotlib.pyplot as plt

MANN_BOX_FMT = '<f'  # binary turbulence datatype


# %%


def extract_wsp_text(case, opt='wsp'):
    """
    Extract the wind speed from the text file, into a float.

    Parameters
    ----------
    case : str
        Turbulence box file name.

    Returns
    -------
    wind_speed : float
        Wind speed from the text file name.

    """

    allowed_options = ['wsp', 'ae', 'seed']
    func.check_allow_opt(opt, allowed_options)

    if opt == 'wsp':
        pattern = r'_wsp_([0-9]+\.[0-9]+)_seed_'
        match = re.search(pattern, case)
        value = float(match.group(1)) 

    elif opt == 'ae':
        pattern = r'_ae_([0-9]+\.[0-9]+)'
        match = re.search(pattern, case)
        value = float(match.group(1)) 

    elif opt == 'seed':
        pattern = r'_seed_([0-9]+\.[0-9]+)'
        match = re.search(pattern, case)
        value = float(match.group(1)) 

    else:
        print('Option is not valid')
    
    return value


# %%

def extract_ae_text(case):
    """
    Extract the wind speed from the text file, into a float.

    Parameters
    ----------
    case : str
        Turbulence box file name.

    Returns
    -------
    ae : float
        Alphaepsilon parameter from Mann-turbulence box.

    """
    pattern = r'_ae_([0-9]+\.[0-9]+)'
    match = re.search(pattern, case)
    ae = float(match.group(1)) 
    
    return ae



# %% Function Generate files names

def generate_fname(name, parts):
    """
    Generates the fname list with all the parts, based on the initial name and
    the amount of parts per file. Return a list.

    """
    
    fname = []

    # To generate the files automatically:
    for i in range(parts):
        file_name = name + f'_part_{i+1}'
        fname.append(file_name)
    
    return fname


# %% Function to initialize the Netcdf file.


def netCDF4_initialization(file_path, compression_level, number_beams,
                           t_start, t_end, deltat):
    """
    Initializes and generate the NETcdf4 file with the correct variables
    names and with float64 values. 
    It generates two groups, one for the aeroelastic response and another 
    one for the hub-lidar measurements. 

    Parameters
    ----------
    file_path : str
        Name of the file name for the NETcdf file. 
    compression_level : int
        Compression level desire for the NETcdf file initialization. 
    number_beams : int
        Total number of beams to be stored from the hub-lidar.
        This is the total number of beams from all the HAWC2 simulations. 

    Returns
    -------
    None.

    """
    
    # Parameters
    chunk_size = 1000  # Chunk size for both dimensions
    init_value = 999.99  # Initial value for the data
    aero_res_output = 119  # Number of aeroelastic channels to be stored.
    
    # Initialization of time
    Time = np.arange(t_start, t_end, deltat)
    
    # To extract variables and metadata information for each variable: 
    from _var_names import var_names_HuLi, var_names_aero, HuLi_metadata, aero_metadata
    
    file_path = './NETcdf/' + file_path
       
    # Create a new NetCDF file
    with Dataset(file_path, 'w', format='NETCDF4') as nc:
    
        # Create groups for the dataset:
        wt_res = nc.createGroup('wt_res')
        HuLi_data = nc.createGroup('HuLi_data')
    
        # Define dimensions for each group
        wt_res.createDimension('time', Time.size)
        wt_res.createDimension('output_channel', 1)  # This is because, each variable is a column.
    
        HuLi_data.createDimension('time', Time.size)
        HuLi_data.createDimension('beam_num', number_beams)
        
        # Create a time variable for each group, based on Time array
        time_var_wt = wt_res.createVariable('time', np.float64, ('time',))
        time_var_wt[:] = Time  # Assign the time values

        time_var_HuLi = HuLi_data.createVariable('time', np.float64, ('time',))
        time_var_HuLi[:] = Time  # Assign the time values

        # Add metadata to the time variable
        time_var_wt.units = 'seconds'
        time_var_wt.description = 'Time from HAWC2 simulation, in seconds.'

        # Add metadata to the time variable
        time_var_HuLi.units = 'seconds'
        time_var_HuLi.description = 'Time from HAWC2 simulation, in seconds'

        # Create variables for wt_res group
        for var_name in var_names_aero: 
            variable = wt_res.createVariable(var_name, np.float64, ('time', 'output_channel'),
                                             zlib=True, complevel=compression_level)
                    
            for start_row in range (0, Time.size, chunk_size):
                end_row = min(start_row + chunk_size, Time.size)
                variable[start_row:end_row, :] = init_value
                print(f'Group: wt_res, Variable: {var_name}, Chunk: {start_row}')
                
            # Set metadata for each variable
            if var_name in aero_metadata:
                variable.units = aero_metadata[var_name]['units']
                variable.description = aero_metadata[var_name]['description']
                
        # Create variables for HuLi_data group
        for var_name in var_names_HuLi: 
            variable = HuLi_data.createVariable(var_name, np.float64, ('time', 'beam_num'),
                                                zlib=True, complevel=compression_level)
            
            for start_row in range (0, Time.size, chunk_size):
                end_row = min(start_row + chunk_size, Time.size)
                variable[start_row:end_row, :] = init_value
                print(f'Group: HuLi_data, Variable: {var_name}, Chunk: {start_row}')
                
            # Set metadata for each variable
            if var_name in HuLi_metadata:
                variable.units = HuLi_metadata[var_name]['units']
                variable.description = HuLi_metadata[var_name]['description']
                        
    return print("NetCDF file created successfully.")


# %% Read HAWC2 output channels from result file.


def read_out_channels(path, fname, t_start, t_end):
    """
    Function to extract main output channels from HAWC2 aeroelastic simulation
    to be added to the NETcdf file. This is only perform on one of the
    simulations for the same inflow case. By default, part 1 is considered. 
    """

    hawc2_path = path + fname[0] + '.hdf5'
    read_h2 = ReadHawc2(hawc2_path)
    hawc2_res = read_h2.ReadGtsdf()


    data_base = {
        'Time': hawc2_res[:, 0],  # s Time
        'shaft_rot_angle': hawc2_res[:, 1],  # deg   shaft_rot angle
        'shaft_rot_angle speed': hawc2_res[:, 2],  # rpm   shaft_rot angle speed
        'pitch1_angle': hawc2_res[:, 3],  # deg   pitch1 angle
        'pitch1_angle speed': hawc2_res[:, 4],  # deg/s   pitch1 angle speed
        'pitch2_angle': hawc2_res[:, 5],  # deg bea2 angle (duplicate, adjust index) -> pitch2 angle
        'pitch2_angle speed': hawc2_res[:, 6],  # deg/s bea2 angle_speed (duplicate, adjust index) -> pitch2 angle speed
        'pitch3_angle': hawc2_res[:, 7],  # deg bea2 angle (duplicate, adjust index) -> pitch3 angle
        'pitch3_angle speed': hawc2_res[:, 8],  # deg/s bea2 angle_speed (duplicate, adjust index) -> pitch3 angle speed
        'Rotor_speed': hawc2_res[:, 9],  # rad/s Omega -> Rotor speed
        'torque': hawc2_res[:, 10],  # kNm Ae rot. torque -> Aero rotor torque
        'Aero_power': hawc2_res[:, 11],  # kW Ae rot. power -> Aero rotor power
        'thrust': hawc2_res[:, 12],  # kN Ae rot. thrust -> Aero rotor thrust
        'fw_Vx': hawc2_res[:, 13],  # m/s WSP gl. coo.,Vx -> Free wind speed Vx, gl. coo.
        'fw_Vy': hawc2_res[:, 14],  # m/s WSP gl. coo.,Vy -> Free wind speed Vy, gl. coo.
        'fw_Vz': hawc2_res[:, 15],  # m/s WSP gl. coo.,Vz -> Free wind speed Vz, gl. coo.
        'WSP_abs_vhor': hawc2_res[:, 16], # m/s Free wind speed absolute vhor 0,0,-119, gl. coo.
        'WSP_Vdir_hor': hawc2_res[:, 17], # deg, Free wind speed Vdir hor, gl. coo. 0, 0, -1190
        'raws': hawc2_res[:, 18],  # m/s WSP rotor avg gl. coo., Vy -> Rotor average free wind speed Vy, global coo.
        'TB_Mx': hawc2_res[:, 19],  # kNm Mx coo: tower (base) -> MomentMx tower base
        'TB_My': hawc2_res[:, 20],  # kNm My coo: tower (base) -> MomentMy tower base
        'TB_Mz': hawc2_res[:, 21],  # kNm Mz coo: tower (base) -> MomentMz tower base
        'TYB_Mx': hawc2_res[:, 22],  # kNm Mx coo: tower (yaw bearing) -> MomentMx tower yaw bearing
        'TYB_My': hawc2_res[:, 23],  # kNm My coo: tower (yaw bearing) -> MomentMy tower yaw bearing
        'TYB_Mz': hawc2_res[:, 24],  # kNm Mz coo: tower (yaw bearing) -> MomentMz tower yaw bearing
        'MB_Mx': hawc2_res[:, 25],  # kNm Mx coo: shaft (main bearing) -> MomentMx main bearing
        'MB_My': hawc2_res[:, 26],  # kNm My coo: shaft (main bearing) -> MomentMy main bearing
        'MB_Mz': hawc2_res[:, 27],  # kNm Mz coo: shaft (main bearing) -> MomentMz main bearing
        'BR1_Mx_ipop': hawc2_res[:, 28],  # kNm Mx coo: hub1 (blade 1 root ipop) -> MomentMx blade1 root ipop
        'BR1_My_ipop': hawc2_res[:, 29],  # kNm My coo: hub1 (blade 1 root ipop) -> MomentMy blade1 root ipop
        'BR1_Mz_ipop': hawc2_res[:, 30],  # kNm Mz coo: hub1 (blade 1 root ipop) -> MomentMz blade1 root ipop
        'BR2_Mx_ipop': hawc2_res[:, 31],  # kNm Mx coo: hub2 (blade 2 root ipop) -> MomentMx blade2 root ipop
        'BR2_My_ipop': hawc2_res[:, 32],  # kNm My coo: hub2 (blade 2 root ipop) -> MomentMy blade2 root ipop
        'BR2_Mz_ipop': hawc2_res[:, 33],  # kNm Mz coo: hub2 (blade 2 root ipop) -> MomentMz blade2 root ipop
        'BR3_Mx_ipop': hawc2_res[:, 34],  # kNm Mx coo: hub3 (blade 3 root ipop) -> MomentMx blade3 root ipop
        'BR3_My_ipop': hawc2_res[:, 35],  # kNm My coo: hub3 (blade 3 root ipop) -> MomentMy blade3 root ipop
        'BR3_Mz_ipop': hawc2_res[:, 36],  # kNm Mz coo: hub3 (blade 3 root ipop) -> MomentMz blade3 root ipop
        'BR1_Mx_flped': hawc2_res[:, 37],  # kNm Mx coo: blade1 (blade 1 root flped) -> MomentMx blade1 root flped
        'BR1_My_flped': hawc2_res[:, 38],  # kNm My coo: blade1 (blade 1 root flped) -> MomentMy blade1 root flped
        'BR1_Mz_flped': hawc2_res[:, 39],  # kNm Mz coo: blade1 (blade 1 root flped) -> MomentMz blade1 root flped
        'BR2_Mx_flped': hawc2_res[:, 40],  # kNm Mx coo: blade2 (blade 2 root flped) -> MomentMx blade2 root flped
        'BR2_My_flped': hawc2_res[:, 41],  # kNm My coo: blade2 (blade 2 root flped) -> MomentMy blade2 root flped
        'BR2_Mz_flped': hawc2_res[:, 42],  # kNm Mz coo: blade2 (blade 2 root flped) -> MomentMz blade2 root flped
        'BR3_Mx_flped': hawc2_res[:, 43],  # kNm Mx coo: blade3 (blade 3 root flped) -> MomentMx blade3 root flped
        'BR3_My_flped': hawc2_res[:, 44],  # kNm My coo: blade3 (blade 3 root flped) -> MomentMy blade3 root flped
        'BR3_Mz_flped': hawc2_res[:, 45],  # kNm Mz coo: blade3 (blade 3 root flped) -> MomentMz blade3 root flped
        'Bld1_50_local_Mx': hawc2_res[:, 46],  # kNm Mx coo: local (blade 1 rotorcenter ipop) -> MomentMx blade1 50% local e coo
        'Bld1_50_local_My': hawc2_res[:, 47],  # kNm My coo: local (blade 1 rotorcenter ipop) -> MomentMy blade1 50% local e coo
        'Bld1_50_local_Mz': hawc2_res[:, 48],  # kNm Mz coo: local (blade 1 rotorcenter ipop) -> MomentMz blade1 50% local e coo
        'TT_pos_x': hawc2_res[:, 49],  # m State pos x coo: global (tower top fa displ) -> State pos x tower top fa displ
        'TT_pos_y': hawc2_res[:, 50],  # m State pos y coo: global (tower top ss displ) -> State pos y tower top ss displ
        'TT_acc_x': hawc2_res[:, 51],  # m/s^2 State acc x coo: global (tower top fa acc) -> State acc x tower top fa acc
        'TT_acc_y': hawc2_res[:, 52],  # m/s^2 State acc y coo: global (tower top ss acc) -> State acc y tower top ss acc
        'Bld1_tip_pos_x': hawc2_res[:, 53],  # m State pos x coo: blade1 (blade 1 tip pos) -> State pos x blade1 tip pos
        'Bld1_tip_pos_y': hawc2_res[:, 54],  # m State pos y coo: blade1 (blade 1 tip pos) -> State pos y blade1 tip pos
        'Bld1_tip_pos_z': hawc2_res[:, 55],  # m State pos z coo: blade1 (blade 1 tip pos) -> State pos z blade1 tip pos
        'Bld2_tip_pos_x': hawc2_res[:, 56],  # m State pos x coo: blade2 (blade 2 tip pos) -> State pos x blade2 tip pos
        'Bld2_tip_pos_y': hawc2_res[:, 57],  # m State pos y coo: blade2 (blade 2 tip pos) -> State pos y blade2 tip pos
        'Bld2_tip_pos_z': hawc2_res[:, 58],  # m State pos z coo: blade2 (blade 2 tip pos) -> State pos z blade2 tip pos
        'Bld3_tip_pos_x': hawc2_res[:, 59],  # m State pos x coo: blade3 (blade 3 tip pos) -> State pos x blade3 tip pos
        'Bld3_tip_pos_y': hawc2_res[:, 60],  # m State pos y coo: blade3 (blade 3 tip pos) -> State pos y blade3 tip pos
        'Bld3_tip_pos_z': hawc2_res[:, 61],  # m State pos z coo: blade3 (blade 3 tip pos) -> State pos z blade3 tip pos
    	'Bld1_tip_gl_pos_x': hawc2_res[:, 62],  # m State pos x coo: blade1 (blade 1 tip pos) -> State pos x blade1 tip Global coordinate pos
        'Bld1_tip_gl_pos_y': hawc2_res[:, 63],  # m State pos y coo: blade1 (blade 1 tip pos) -> State pos y blade1 tip Global coordinate pos
        'Bld1_tip_gl_pos_z': hawc2_res[:, 64],  # m State pos z coo: blade1 (blade 1 tip pos) -> State pos z blade1 tip Global coordinate pos
        'hub1_pos_x': hawc2_res[:, 65],  # m State pos x coo: global (hub1 global pos) -> State pos x hub1 global pos
        'hub1_pos_y': hawc2_res[:, 66],  # m State pos y coo: global (hub1 global pos) -> State pos y hub1 global pos
        'hub1_pos_z': hawc2_res[:, 67],  # m State pos z coo: global (hub1 global pos) -> State pos z hub1 global pos
        'hub1_vel_x': hawc2_res[:, 68],  # m/s State vel x coo: global (hub1 global velocity) -> State vel x hub1 global velocity
        'hub1_vel_y': hawc2_res[:, 69],  # m/s State vel y coo: global (hub1 global velocity) -> State vel y hub1 global velocity
        'hub1_vel_z': hawc2_res[:, 70],  # m/s State vel z coo: global (hub1 global velocity) -> State vel z hub1 global velocity
        'Bld1_Vy': hawc2_res[:, 71],  # m/s WSP Vy, glco, R= 72.3 -> Wind speed Vy of blade 1 at radius 72.33, global coo.
        'Bld1_Angle_attack': hawc2_res[:, 72],  # deg Alfa, R= 72.3 for blade 1 -> Angle of attack of blade 1 at radius 72.33
        'Bld2_Angle_attack': hawc2_res[:, 73],  # deg Alfa, R= 72.3 for blade 2 -> Angle of attack of blade 2 at radius 72.33
        'Bld3_Angle_attack': hawc2_res[:, 74],  # deg Alfa, R= 72.3 for blade 3 -> Angle of attack of blade 3 at radius 72.33
        'Bld1_Cl': hawc2_res[:, 75],  # Cl, R= 72.5 for blade 1 -> Cl of blade 1 at radius 72.33
        'Bld2_Cl': hawc2_res[:, 76],  # Cl, R= 72.5 for blade 2 -> Cl of blade 2 at radius 72.33
        'Bld3_Cl': hawc2_res[:, 77],  # Cl, R= 72.5 for blade 3 -> Cl of blade 3 at radius 72.33
        'Bld1_Cd': hawc2_res[:, 78],  # Cd, R= 72.5 for blade 1 -> Cd of blade 1 at radius 72.33
        'Bld2_Cd': hawc2_res[:, 79],  # Cd, R= 72.5 for blade 2 -> Cd of blade 2 at radius 72.33
        'Bld3_Cd': hawc2_res[:, 80],  # Cd, R= 72.5 for blade 3 -> Cd of blade 3 at radius 72.33
        
        # DLL Channels: 
        'DLL_torque_ref': hawc2_res[:, 81],  # DLL inp 1: 1 -> DLL generator torque reference [nm]
        'DLL_bld1_pitch_angle_ref': hawc2_res[:, 82],  # DLL inp 1: 2 -> DLL pitch angle reference of blade 1 [rad]
        'DLL_bld2_pitch_angle_ref': hawc2_res[:, 83],  # DLL inp 1: 3 -> DLL pitch angle reference of blade 2 [rad]
        'DLL_bld3_pitch_angle_ref': hawc2_res[:, 84],  # DLL inp 1: 4 -> DLL pitch angle reference of blade 3 [rad]
        'DLL_power_ref': hawc2_res[:, 85],  # DLL inp 1: 5 -> DLL power reference [w]
        'DLL_filter_wsp': hawc2_res[:, 86],  # DLL inp 1: 6 -> DLL filtered wind speed [m/s]
        'DLL_filter_rot_speed': hawc2_res[:, 87],  # DLL inp 1: 7 -> DLL filtered rotor speed [rad/s]
        'DLL_filter_rot_speed_error_torque': hawc2_res[:, 88],  # DLL inp 1: 8 -> DLL filtered rotor speed error for torque [rad/s]
        'DLL_bandpass_filter_rot_speed': hawc2_res[:, 89],  # DLL inp 1: 9 -> DLL bandpass filtered rotor speed [rad/s]
        'DLL_prop_torque_con': hawc2_res[:, 90],  # DLL inp 1: 10 -> DLL proportional term of torque controller [nm]
        'DLL_int_torque_controller': hawc2_res[:, 91],  # DLL inp 1: 11 -> DLL integral term of torque controller [nm]
        'DLL_min_lim_torque': hawc2_res[:, 92],  # DLL inp 1: 12 -> DLL minimum limit of torque [nm]
        'DLL_max_lim_torque': hawc2_res[:, 93],  # DLL inp 1: 13 -> DLL maximum limit of torque [nm]
        'DLL_torque_lim_switch_pitch': hawc2_res[:, 94],  # DLL inp 1: 14 -> DLL torque limit switch based on pitch [-]
        'DLL_filter_rot_speed_error_pitch': hawc2_res[:, 95],  # DLL inp 1: 15 -> DLL filtered rotor speed error for pitch [rad/s]
        'DLL_power_error_pitch': hawc2_res[:, 96],  # DLL inp 1: 16 -> DLL power error for pitch [w]
        'DLL_prop_pitch_con': hawc2_res[:, 97],  # DLL inp 1: 17 -> DLL proportional term of pitch controller [rad]
        'DLL_int_pitch_controller': hawc2_res[:, 98],  # DLL inp 1: 18 -> DLL integral term of pitch controller [rad]
        'DLL_min_lim_pitch': hawc2_res[:, 99],  # DLL inp 1: 19 -> DLL minimum limit of pitch [rad]
        'DLL_max_lim_pitch': hawc2_res[:, 100],  # DLL inp 1: 20 -> DLL maximum limit of pitch [rad]
        'DLL_torque_ref_damper': hawc2_res[:, 101],  # DLL inp 1: 21 -> DLL torque reference from dt damper [nm]
        'DLL_status_signal': hawc2_res[:, 102],  # DLL inp 1: 22 -> DLL status signal [-]
        'DLL_total_added_pitch_rate': hawc2_res[:, 103],  # DLL inp 1: 23 -> DLL total added pitch rate [rad/s]
        'DLL_filter_mean_pitch': hawc2_res[:, 104],  # DLL inp 1: 24 -> DLL filtered mean pitch for gain schedule [rad]
        'DLL_flag_mech_brake': hawc2_res[:, 105],  # DLL inp 1: 25 -> DLL flag for mechanical brake [0=off/1=on]
        'DLL_flag_emergency_pitch_stop': hawc2_res[:, 106],  # DLL inp 1: 26 -> DLL flag for emergency pitch stop [0=off/1=on]
        'DLL_lp_filter_acc_level': hawc2_res[:, 107],  # DLL inp 1: 27 -> DLL lp filtered acceleration level [m/s^2]
        'DLL_mon_avg_ref_pitch': hawc2_res[:, 108],  # DLL inp 1: 31 -> DLL monitored average of reference pitch [rad]
        'DLL_mon_avg_actual_pitch_blade1': hawc2_res[:, 109],  # DLL inp 1: 32 -> DLL monitored average of actual pitch (blade 1) [rad]
        'DLL_mgen_lss': hawc2_res[:, 110],  # DLL inp 2: 1 -> DLL mgen lss [nm]
        'DLL_pelec': hawc2_res[:, 111],  # DLL inp 2: 2 -> DLL pelec [w]
        'DLL_mframe': hawc2_res[:, 112],  # DLL inp 2: 3 -> DLL mframe [nm]
        'DLL_mgen_hss': hawc2_res[:, 113],  # DLL inp 2: 4 -> DLL mgen hss [nm]
        'DLL_grid_flag': hawc2_res[:, 114],  # DLL inp 2: 8 -> DLL grid flag [0=run/1=stop]
        'DLL_brake_torque': hawc2_res[:, 115],  # DLL inp 3: 1 -> DLL brake torque [nm]
        'DLL_pitch1': hawc2_res[:, 116],  # DLL inp 4: 1 -> DLL pitch 1 [rad]
    	'DLL_pitch2': hawc2_res[:, 117],  # DLL inp 4: 1 -> DLL pitch 2 [rad]
    	'DLL_pitch3': hawc2_res[:, 118],  # DLL inp 4: 1 -> DLL pitch 3 [rad]
    	'DLL_Bld_tip_tow': hawc2_res[:, 119], # DLL :  5 inpvec :   1 min. distance bladetips tower [m]
    	}
    
    df = pd.DataFrame(data_base)
    
    # To filter the DataFrame by the selected time period:
    ds_data_base = df[(df['Time'] >= t_start) & (df['Time'] <= t_end)]
    #ds_data_base.to_xarray()
    
    return ds_data_base


#%% Find time indices for the selected time period:


def find_time_indices(time_series, t_start, t_end):
    """
    Finds indices in the time series closest to the specified start 
    and end times.
    """

    start_diff = np.abs(time_series - t_start)
    end_diff = np.abs(time_series - t_end)
    
    start_index = np.argmin(start_diff)
    end_index = np.argmin(end_diff)
    
    return start_index, end_index


# %% Function to update the NETcdf file with the aeroelastic turbine response:


def update_wt_res(file_path, df_wt_res):
    """
    Takes the initilized NETcdf file, and update the aeroelastic response
    channels with the data from HAWC2 simulations. 

    Parameters
    ----------
    file_path : str
        NETcdf file name.
    df_wt_res : DataFrame
        Dataframe with all the aeroelastic response data from HAWC2
        simulations.

    Returns
    -------
    None.

    """
    
    file_path = './NETcdf/' + file_path
    
    with Dataset(file_path, 'a') as nc:  # Open the file in append mode
        wt_res = nc.groups['wt_res']
        
        for column in df_wt_res.columns:
            variable = wt_res.variables[column]
            data = df_wt_res[column].values
            variable[:] = data.reshape(-1, 1)  # Reshape needed if dimension is (time, 1)
        print("Aeroelastic response channels has been updated successfully on wt_res group.")
    

# %% Function to update HuLi_data group:


def update_huli_data(nc_path, path, fname,
                     num_beams, total_beams, 
                     t_start=100.05, t_end=700.05, idx_initial=120):
    """
    Update the initialize NETcdf file, with the Hub-lidar data from 
    HAWC2 simulations. Since multiple files containt all the results, the data
    is appended for each file available.

    Parameters
    ----------
    nc_path : str
        NETcdf file name.
    path : str
        Path for the HAWC2 simulations results are stored.
    fname : list
        Contains the file names of each HAWC2 simulations results.
    num_beams : int
        Number of hub-lidar beams per simulation. Set by default to 2000.
    total_beams : int
        Total number of beams across all the simulations. Basically, all
        possible configurations based on the initial inputs. 
        Set by default to 
    t_start : float
        Time start for the data should be stored. Set by default to 100.05 s.
    t_end : float
        Time end for the data should be stored. Set by default as 700.05 s.
    idx_initial : int
        First index of the hub-lidar channel in HAWC2 for the Hub-lidar beam.
        Set by default to 120.

    Returns
    -------
    None.

    """
    step_size = 8  # Hub-lidar output channels in HAWC2.
    
    file_path = './NETcdf/' + nc_path

    with nc.Dataset(file_path, 'a') as dataset:
        huli_group = dataset.groups['HuLi_data']
        
        # Precompute all indices first
        indices = np.arange(idx_initial, idx_initial + num_beams * step_size, step_size)
        
        for j, name in enumerate(fname):
            print(f'Reading file: {name}.')
            hawc2_path = path + name + '.hdf5'
            
            read_h2 = ReadHawc2(hawc2_path)
            hawc2_res = read_h2.ReadGtsdf()

            Time = hawc2_res[:, 0]
            start_idx, end_idx = find_time_indices(Time, t_start, t_end)
            time_slice = slice(start_idx, end_idx)

            start_beam = j * num_beams
            end_beam = min(start_beam + num_beams, total_beams)

            # Processing all beam data at once for each file
            for var_offset, var_name in enumerate(['HuLi_Xg', 'HuLi_Yg', 'HuLi_Zg', 'HuLi_V_LOS_wgh', 'HuLi_V_LOS_nom']):
                all_data = np.array([hawc2_res[time_slice, idx + var_offset] for idx in indices[:end_beam-start_beam]])
                huli_group.variables[var_name][:, start_beam:end_beam] = all_data.T

        print("HuLi data group in the NetCDF file updated successfully.")



# %%


def generate_Cases(**kw_turbgen):
    """
    Generates the list with all the cases, based on the wind speed and seed
    from the dictionaries initially defined.

    Parameters
    ----------
    **kw_turbgen : dict
        Dictionary containing the information for turbulence box generation.

    Returns
    -------
    Cases : list
        List with all the file names based on inflow condition for 
        Mann-generated turbulence boxes.

    """
    
    wsps = kw_turbgen['wsps']
    Seeds = kw_turbgen['Seeds']
    Cases = []

    for wsp in wsps:    
        for seed in Seeds:
                               
            # Based on the IEC Standard:
            sigma = kw_turbgen['I_ref'] * (0.75 * wsp + 5.6)
            variance = sigma**2
            
            sample_frq = 1/(kw_turbgen['Time']/kw_turbgen['Nx'])  

            ae = var2ae(variance, kw_turbgen['L'],
                        kw_turbgen['gamma'], wsp,
                        T=kw_turbgen['Time'],
                        sample_frq=sample_frq, plt=False)
                        
            # For Turbulence Box Generation:
            file_name = kw_turbgen['turb_model'] + '_wsp_' + str(wsp) + '_seed_' + str(seed) + f'_ae_{ae:.3f}'
            
            Cases.append(file_name)
    
    return Cases



# %% Calculates the number of files parts required for beam data:


def calc_part_num(df_lidar, num_beam_per_file):
    """
    Calculates the number of files part required to include all the beam
    outputs in HAWC2, and therefore, the number of result files that must be
    read.

    Parameters
    ----------
    df_lidar : DataFrame
        DataFrame with all the beam data from HAWC2 simulations.
    num_beam_per_file : int
        Number of beams per file.
    
    Returns
    -------
    int
        Number of files parts required to include all the beam outputs.
    """
    return int(np.ceil(df_lidar.shape[0]/num_beam_per_file))


# %%  Add turbulence box values from HAWC2 to the NetCDF file:
# Deprecated function, use add_turbbox_to_netcdf instead.

def add_turbbox_to_netcdf(case, turb_box_metadata, t_start=100.05, t_end=700, **kw_turbgen):
    """
    Add the turbulence box information to the NetCDF file, based on the
    information from the HAWC2 binary files.

    Parameters
    ----------
    case : str
        File name of the inflow case to be added.   
    turb_box_metadata : dict
        Dictionary with the metadata information for the turbulence box.
    **kw_turbgen : dict
        Dictionary with all the information for the turbulence box generation.
    
    Returns
    -------
    None.    

    """
    
    # Extract wind speed:    
    wsp = extract_wsp_text(case)
    
    file_path = './NETcdf/' + case + '.nc'
    
    print(f'Reading file: {case}, with wind speed: {wsp} [m/s].')

    # Reading HAWC2 result file:         
    print(f'Reading file: {case}.')
    path_res = './dtu_10mw/res/'  # Path for the HAWC2 results
    hawc2_path = path_res + case + '.hdf5'
    read_h2 = ReadHawc2(hawc2_path)
    hawc2_res = read_h2.ReadGtsdf()

    # To extract based on the simulation time: 
    Time = hawc2_res[:, 0]
    
    # Open the existing NetCDF file in append mode
    dataset = nc.Dataset(file_path, 'a')

    # Create the 'turb_box' group if not already created
    if 'turb_box' not in dataset.groups:
        turb_box_group = dataset.createGroup('turb_box')
    else:
        turb_box_group = dataset.groups['turb_box']
    
    # Create dimensions within the 'turb_box' group
    t = turb_box_group.createDimension('t', Time.size) 
    x = turb_box_group.createDimension('Xg', kw_turbgen['Ny'])
    z = turb_box_group.createDimension('Zg', kw_turbgen['Nz'])

    # Time dimension (equivalent to longitudinal direction):
    t_var = turb_box_group.createVariable('t', 'f8', ('t',))
    t_var[:] = Time
       
    # Lateral direction:
    x_var = turb_box_group.createVariable('Xg', 'f8', ('Xg',))
    x_var[:] = np.linspace(-kw_turbgen['Ly']/2, kw_turbgen['Ly']/2, kw_turbgen['Ny'])
    
    # Vertical direction:
    z_var = turb_box_group.createVariable('Zg', 'f8', ('Zg',))
    z_var[:] = np.linspace(0, -kw_turbgen['Lz'], kw_turbgen['Nz'])
    
    # Create variables for u, v, and w
    u = turb_box_group.createVariable('u', 'f8', ('t', 'Xg', 'Zg'))  # u component with added shear
    v = turb_box_group.createVariable('v', 'f8', ('t', 'Xg', 'Zg'))  # v component
    w = turb_box_group.createVariable('w', 'f8', ('t', 'Xg', 'Zg'))  # w component

    # Assign data to each spatial point
    for x_idx in range(kw_turbgen['Ny']):
        for z_idx in range(kw_turbgen['Nz']):
            # Index reading for the HAWC2 beam data:
            idx = 19 + (x_idx * kw_turbgen['Nz'] + z_idx) * 3
            
            # Assign data to the variables:
            u[:, x_idx, z_idx] = hawc2_res[:, idx + 1]  # u component
            v[:, x_idx, z_idx] = hawc2_res[:, idx]      # v component
            w[:, x_idx, z_idx] = hawc2_res[:, idx + 2]  # w component


    #for init in range(0, (kw_turbgen['Ny'] * kw_turbgen['Nz'])):
        # Index reading for the HAWC2 beam data:
        # idx = 19+(3*init)

    print("Turbulence box info from HAWC2 has been updated successfully in NETcdf file.")

    # Adding Metadata to the groups variables and dimensions:
    turb_box_group.variables['t'].setncatts(turb_box_metadata['t'])
    turb_box_group.variables['Xg'].setncatts(turb_box_metadata['Xg'])
    turb_box_group.variables['Zg'].setncatts(turb_box_metadata['Zg'])
    
    # Update metadata for data variables
    turb_box_group.variables['u'].setncatts(turb_box_metadata['u'])
    turb_box_group.variables['v'].setncatts(turb_box_metadata['v'])
    turb_box_group.variables['w'].setncatts(turb_box_metadata['w'])
       
    print(f'Data has been saved in {case}. \n')

    # Close the file to save changes
    dataset.close()

    return print(f'Turbulence box {case} has been added to NetCDF file.')


# %% Function to add Mann turbulence box in Mann coordinate system:


def add_mann_turbbox_to_netcdf(case, mann_tbox_metadata, dxt, deltat, **kw_turbgen):
    """
    Add the turbulence box information to the NetCDF file, based on the
    information from the HAWC2 binary files.

    Parameters
    ----------
    case : str
        File name of the inflow case to be added.   
    mann_tbox_metadata : dict
        Dictionary with the metadata information for the Mann turbulence box.
    dxt : float
        Spatial resolution in the Yg-direction.
    deltat : float
        Temporal resolution in the Yg-direction.
    **kw_turbgen : dict
        Dictionary with all the information for the turbulence box generation.
    
    Returns
    -------
    None.    

    """
    
    # Extract wind speed:    
    wsp = extract_wsp_text(case)
    
    file_path = './NETcdf/' + case + '.nc'
    
    print(f'Reading file: {case}, with wind speed: {wsp} [m/s].')
    
    # Open the existing NetCDF file in append mode
    dataset = nc.Dataset(file_path, 'a')
    
    # Create the 'turb_box' group
    turb_box_group = dataset.createGroup('mann_tbox')
    
    # Create dimensions within the 'turb_box' group
    x = turb_box_group.createDimension('x', kw_turbgen['Nx'])
    y = turb_box_group.createDimension('y', kw_turbgen['Ny'])
    z = turb_box_group.createDimension('z', kw_turbgen['Nz'])
    
    # Create variables for dimensions with their respective data
    x_var = turb_box_group.createVariable('x', 'f8', ('x',))
    x_var[:] = np.linspace(0, dxt * kw_turbgen['Nx'], kw_turbgen['Nx'])
    
    y_var = turb_box_group.createVariable('y', 'f8', ('y',))
    y_var[:] = np.linspace(kw_turbgen['Ly']/2, -kw_turbgen['Ly']/2, kw_turbgen['Ny'])
    
    z_var = turb_box_group.createVariable('z', 'f8', ('z',))
    z_var[:] = -np.linspace(0, kw_turbgen['Lz'], kw_turbgen['Nz'])

    # Due to the coordinate system in HAWC2, the time is reversed:
    t_var = turb_box_group.createVariable('t', 'f8', ('x', 'y', 'z'))
    time_1d = np.flip(np.linspace(deltat, deltat * kw_turbgen['Nx'], kw_turbgen['Nx']))
    # repeats the 1D array along YZ direction (XgZg plane):
    time_3d = np.tile(time_1d[:, np.newaxis, np.newaxis], (1, kw_turbgen['Ny'], kw_turbgen['Nz']))

    # Assign the expanded 3D time array to 't_var'
    t_var[:] = time_3d

    # Create variables for u, v, and w
    u = turb_box_group.createVariable('u', 'f8', ('x', 'y', 'z'))
    v = turb_box_group.createVariable('v', 'f8', ('x', 'y', 'z'))
    w = turb_box_group.createVariable('w', 'f8', ('x', 'y', 'z'))
    u_shear = turb_box_group.createVariable('u_shear', 'f8', ('x', 'y', 'z'))

    
    # Read turbulence Box and stored it in NETcdf file:
    path = './dtu_10mw/turb/'
    u[:], v[:], w[:] = tbox.read_windsimu(path, case, 
                                    kw_turbgen['Nx'], 
                                    kw_turbgen['Ny'],
                                    kw_turbgen['Nz'], opt='all')
    
    # To read turbulence box with added shear profile: 
    u_shear[:] = tbox.read_windsimu(path, case, 
                                    kw_turbgen['Nx'], 
                                    kw_turbgen['Ny'],
                                    kw_turbgen['Nz'], 
                                    opt='u', shear=True)
    
    print(f'Updating metadata for file: {case}')
    
    # Adding Metadata to the groups variables and dimensions:
    turb_box_group.variables['x'].setncatts(mann_tbox_metadata['x'])
    turb_box_group.variables['y'].setncatts(mann_tbox_metadata['y'])
    turb_box_group.variables['z'].setncatts(mann_tbox_metadata['z'])
    turb_box_group.variables['t'].setncatts(mann_tbox_metadata['t'])
    
    # Update metadata for data variables
    turb_box_group.variables['u'].setncatts(mann_tbox_metadata['u'])
    turb_box_group.variables['v'].setncatts(mann_tbox_metadata['v'])
    turb_box_group.variables['w'].setncatts(mann_tbox_metadata['w'])
    turb_box_group.variables['u_shear'].setncatts(mann_tbox_metadata['u_shear'])
       
    print(f'Data has been saved in {case}. \n')

    # Close the file to save changes
    dataset.close()

    return print(f'Turbulence box {case} has been added to NetCDF file.')



# %% Test correct adition of the turbulence box information into NetCDF file:


def test_turbbox_netcdf(case, dxt, kw_turbgen):
    """
    Test if the turbulence box information from the binary files match
    correctly the inputs saved on the NETcdf file, group: 'turb_box'.

    Parameters
    ----------
    case : str
        File name of the inflow case to be tested.
    kw_turbgen : dict
        Dictionary with all the information for the turbulence box generation.

    Returns
    -------
    None. 

    """
    
    # Extract wind speed:    
    wsp = extract_wsp_text(case)
    
    file_path = './NETcdf/' + case + '.nc'
    path = './dtu_10mw/turb/'
    
    # Define original data for comparison based on wind speed: 
    original_X = np.linspace(0, dxt * kw_turbgen['Nx'], kw_turbgen['Nx'])
    original_Y = np.linspace(200, -200, kw_turbgen['Ny'])
    original_Z = -np.linspace(0, 400, kw_turbgen['Nz'])

    dataset = nc.Dataset(file_path, 'r')  # 'r' is for read mode

    # Access the 'turb_box' group
    turb_box_group = dataset.groups['mann_tbox']

    # Read dimension variables
    X = turb_box_group.variables['x'][:]
    Y = turb_box_group.variables['y'][:]
    Z = turb_box_group.variables['z'][:]

    # Read data variables
    u = turb_box_group.variables['u'][:]
    v = turb_box_group.variables['v'][:]
    w = turb_box_group.variables['w'][:]
    u_shear = turb_box_group.variables['u_shear'][:]

    # Read original turbulence box:
    orig_u, orig_v, orig_w = tbox.read_windsimu(path, case,
                                           kw_turbgen['Nx'], 
                                           kw_turbgen['Ny'],
                                           kw_turbgen['Nz'], opt='all')
    
    orig_u_shear = tbox.read_windsimu(path, case,
                                           kw_turbgen['Nx'], 
                                           kw_turbgen['Ny'],
                                           kw_turbgen['Nz'], opt='u', shear=True)

    # Function to check if arrays are equal
    def arrays_equal(a, b):
        return np.allclose(a, b)

    # Check if the data matches
    print(f'For case: {case}:')
    print("X data matches:", arrays_equal(X, original_X))
    print("Y data matches:", arrays_equal(Y, original_Y))
    print("Z data matches:", arrays_equal(Z, original_Z))
    print("u data matches:", arrays_equal(u, orig_u))
    print("v data matches:", arrays_equal(v, orig_v))
    print("w data matches:", arrays_equal(w, orig_w))
    print("u_shear data matches:", arrays_equal(u_shear, orig_u_shear))

    # Close the dataset
    dataset.close()
    
    return print('\nTest completed.')


# %% Function to test Mann-turbulence box from HAWC2 results:

def test_turbbox_hawc2_netcdf(case, path_res, path_turb, idx_initial, deltat, 
                              t_start=100.05, t_end=700, num_check=32,
                              **kw_turbgen):

    # read the turbulence box from HAWC2 results:
    hawc2_path = path_res + case + '_fw' + '.hdf5'
    read_h2 = ReadHawc2(hawc2_path)
    hawc2_res = read_h2.ReadGtsdf()

    # Extract wind speed:
    wsp = extract_wsp_text(case)

    # Comparing cruz reading with the original data:
    Time = hawc2_res[:, 0]
    start_idx, end_idx = find_time_indices(Time, t_start, t_end)
    time_slice = slice(start_idx, end_idx)

    # Original turbulence box for ~819 seconds simulation:
    orig_u, orig_v, orig_w = tbox.read_windsimu(path_turb, case,
                                           kw_turbgen['Nx'], 
                                           kw_turbgen['Ny'],
                                           kw_turbgen['Nz'], opt='all', shear=True)
        
    Xg = np.linspace(-kw_turbgen['Ly']/2, kw_turbgen['Ly']/2, kw_turbgen['Ny'])
    Zg = np.linspace(0, -kw_turbgen['Lz'], kw_turbgen['Nz'])

    idx_array = []
    idx_pos_x = []
    idx_pos_z = []

    for x_idx, x_pos in enumerate(Xg):
        for z_idx, z_pos in enumerate(Zg):
           
            # Index array for HAWC2 time series extraction:
            idx_array.append(idx_initial + 3 * (x_idx * kw_turbgen['Nz'] + z_idx))
            # Index array for turbulence box position:
            idx_pos_x.append(x_idx)
            idx_pos_z.append(z_idx) 

    # To get the arrays we want to check from HAWC2 simulation:
    idx_to_check = np.array(idx_array[::num_check]) 
    idx_x_check = np.array(idx_pos_x[::num_check])
    idx_z_check = np.array(idx_pos_z[::num_check])  

    time_comp = np.linspace(deltat, deltat * kw_turbgen['Nx'], kw_turbgen['Nx'])

    # Time series from NETcdf file:
    file_path = './NETcdf/' + case + '.nc'
    dataset = nc.Dataset(file_path, 'r')  # 'r' is for read mode
    turb_box_group = dataset.groups['mann_tbox']

    # To check the selected values:
    for idx, (check, idx_x, idx_z) in enumerate(zip(idx_to_check, idx_x_check, idx_z_check)):

        # Time series of wind speed components at different positions:
        u = hawc2_res[:, check + 1]
        v = hawc2_res[:, check]
        w = hawc2_res[:, check + 2]
        
        # Time series from original turbulence box:
        u_comp = orig_u[time_slice, idx_x, idx_z]
        v_comp = orig_v[time_slice, idx_x, idx_z]
        w_comp = orig_w[time_slice, idx_x, idx_z]

        # Close the dataset
        u_comp = np.flip(u_comp)
        v_comp = np.flip(v_comp)
        w_comp = -np.flip(w_comp)

        # We need to flip because are saved as the turbulence box: 
        u_netcdf = np.flip(turb_box_group.variables['u_shear'][:, idx_x, idx_z])
        v_netcdf = np.flip(turb_box_group.variables['v'][:, idx_x, idx_z])
        w_netcdf = -np.flip(turb_box_group.variables['w'][:, idx_x, idx_z])
        time_netcdf = np.flip(turb_box_group.variables['t'][:, idx_x, idx_z])

        u_netcdf = np.interp(Time, time_netcdf, u_netcdf)
        v_netcdf = np.interp(Time, time_netcdf, v_netcdf)
        w_netcdf = np.interp(Time, time_netcdf, w_netcdf)

        u_comp_interpolated = np.interp(Time, time_comp, u_comp)
        v_comp_interpolated = np.interp(Time, time_comp, v_comp)
        w_comp_interpolated = np.interp(Time, time_comp, w_comp)

        plt.figure(figsize=(15, 8))

        arrays_equal = np.allclose(u_comp_interpolated, u, atol=1e-4)
        net_equal = np.allclose(u_netcdf, u, atol=1e-4)
        title = f'u component: {arrays_equal} and {net_equal}'
        plot_series(u_comp_interpolated, u, u_netcdf, Time, 
                    title, subplot_index=1)
    
        arrays_equal = np.allclose(v_comp_interpolated, v, atol=1e-4)
        net_equal = np.allclose(v_netcdf, v, atol=1e-4)
        title = f'v component: {arrays_equal} and {net_equal}'
        plot_series(v_comp_interpolated, v, v_netcdf, Time, 
                    title, subplot_index=2)
    
        arrays_equal = np.allclose(w_comp_interpolated, w, atol=1e-4)
        net_equal = np.allclose(w_netcdf, w, atol=1e-4)
        title = f'w component: {arrays_equal} and {net_equal}'
        plot_series(w_comp_interpolated, w, w_netcdf, Time, 
                    title, subplot_index=3)
    
        path_fig = './Figures/turb_box_test/'
        fig_name = '0' + str(idx) + '.png'
        full_path = os.path.join(path_fig, fig_name)
        plt.savefig(full_path)
        print(f"Figure saved to {full_path}")

        #plt.show()
        plt.close('all')

    # Close the dataset
    dataset.close()
        
    return print('Test completed.')

# %%


def plot_series(u_orig, u_hawc2, u_netcdf, time_hawc2, 
                title, subplot_index):
    """
    Plot the comparison of the time series between the turbulence box and HAWC2 results.
    """
    # Plot the interpolated u_random time series
    plt.subplot(3, 1, subplot_index)
    plt.plot(time_hawc2, u_orig, label='Mann')
    plt.plot(time_hawc2, u_hawc2, label='HAWC2', linestyle='--')
    plt.plot(time_hawc2, u_netcdf, label='NETcdf', linestyle='-.')
    plt.legend(loc='upper right')
    plt.xlim([time_hawc2[0], time_hawc2[-1]])

    if subplot_index == 3:
        plt.xlabel('Time [s]', fontsize=12, fontweight='bold')

    plt.ylabel('Wind Speed [m/s]', fontsize=12, fontweight='bold')
    plt.title(title, fontsize=14, fontweight='bold')
    plt.tight_layout()


# %%  Check metadata function in NetCDF file:


def check_metadata(case, group_name, pause_every=50):
    """
    Function to check the metadata of the NETcdf file, and print the structure.

    Parameters
    ----------
    case: str
        File name of the inflow case to be tested.
    group_name: str
        Name of the group to be checked.
    pause_every: int
        Number of lines to pause after printing. Default is 50.
    """
       
    file_path = './NETcdf/' + case + '.nc'
    line_count = 0

    # To pause every pause_every lines:
    def maybe_pause():
        nonlocal line_count
        line_count += 1
        if line_count % pause_every == 0:
            input("Press Enter to continue...")

    with Dataset(file_path, 'r') as nc:
        # Access the specific group
        group = nc.groups[group_name]

        print(f"Structure of the group '{group_name}':\n")
        maybe_pause()

        # Print dimensions of the group
        print("Dimensions:")
        for dim_name, dimension in group.dimensions.items():
            print(f"\t{dim_name}: length {dimension.size}")
            maybe_pause()

        # Print variables of the group
        print("\nVariables:")
        for var_name, variable in group.variables.items():
            print(f"\t{var_name}: shape {variable.shape} | type {variable.dtype}")
            maybe_pause()

            # Print attributes of the variable
            print("\tAttributes:")
            for attr_name in variable.ncattrs():
                print(f"\t\t{attr_name}: {variable.getncattr(attr_name)}")
            print()  # Extra newline for better readability
            maybe_pause()
            
    return

# %% Function to compare turbulence boxes from HAWC2 and NETcdf file:

def compare_tboxes(case, path_turb, path_new_turb, **kwargs):
    """
    Compare the turbulence boxes from HAWC2 and NETcdf file.

    Parameters
    ----------
    case : str
        File name of the inflow case to be tested.
    path_turb : str
        Path for the original turbulence boxes.
    path_new_turb : str
        Path for the turbulence boxes extracted from NETcdf file.
    
    Returns
    -------
    None.

    """
    
    # Read original turbulence box:
    orig_u, orig_v, orig_w = tbox.read_windsimu(path_turb, case,
                                                kwargs['Nx'], 
                                                kwargs['Ny'],
                                                kwargs['Nz'], opt='all', shear=False)
    
    orig_u_shear = tbox.read_windsimu(path_turb, case,
                                                kwargs['Nx'], 
                                                kwargs['Ny'],
                                                kwargs['Nz'], opt='u', shear=True)
    
    # Read new turbulence box:
    new_u, new_v, new_w = tbox.read_windsimu(path_new_turb, case,
                                             kwargs['Nx'], 
                                             kwargs['Ny'],
                                             kwargs['Nz'], opt='all', shear=False)
    
    new_u_shear = tbox.read_windsimu(path_new_turb, case,
                                    kwargs['Nx'], 
                                    kwargs['Ny'],
                                    kwargs['Nz'], opt='u', shear=True)
    
    # Compare the two turbulence boxes:
    print(f'U: {np.array_equal(orig_u, new_u)}')
    print(f'V: {np.array_equal(orig_v, new_v)}')
    print(f'W: {np.array_equal(orig_w, new_w)}')
    print(f'U shear: {np.array_equal(orig_u_shear, new_u_shear)}')
    
    return print('Comparison has been done.')