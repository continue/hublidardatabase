
'''
@author: espa

Functions to interpolate the turbulence box values to match the HAWC2 results.
'''

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import hulidb.database_generation as dgen
import hulidb.turbbox as tbox
import hulidb.functions as func
import hulidb.functions_NETcdf as fNet
from scipy.interpolate import RegularGridInterpolator
from scipy.interpolate import interp1d
from wetb.wind.turbulence.mann_parameters import var2ae
from wetb.hawc2.Hawc2io import ReadHawc2


# %%


def calc_dxt(step, wsp, tstop):
    """
    Calculate the grid size and the number of grid points 
    based on the given parameters.

    Parameters:
    - step (int): Number of steps between each second of the turbulence box.
    - wsp (float): Average wind speed in m/s.
    - tstop (float): Total simulation time in seconds.

    Returns:
    - dxt (float): Size step for the longitudinal direction for turbulence box.
    - Nx (float): Minimum grid points in longitudinal direction.

    """
    dxt = wsp / step  # Based on size for distance in 1 second, divided by the steps at each second.
    Nx = (tstop * wsp) / dxt
    # HAWC2 time step read in seconds:
    deltat = 1 / step

    print(f'The grid size must be at least {Nx}.')

    return dxt, Nx, deltat


def calculate_dx(Nx, time, wsp, opt='up'):
    """
    Calculate the value of dx based on the given parameters,
    for the turbulence box longitudinal direction.

    Parameters:
    - Nx (float): Number of grid points in the longitudinal direction.
    - time (float): Total simulation time.
    - wsp (float): Average wind speed in m/s.
    - opt (str, optional): Rounding option. Default is 'down'.

    Returns:
    - rounded_dx (float): Delta x, or grid size in longitudinal
                              direction.
    - time_new (float): Total time based on rounded dx.

    Raises:
    - ValueError: If the rounding option is not 'up' or 'down'.

    """
    
    allowed_options = ['up', 'down']
    func.check_allow_opt(opt, allowed_options)

    # Calculate dxt for turbulence box longitudinal grid size:
    dx = (time * wsp) / Nx
    # Round up to one decimal place
    if opt == 'up':
        rounded_dx = np.ceil(dx * 10) / 10.0
    elif opt == 'down':
        rounded_dx = np.floor(dx * 10) / 10.0
    # New time based on the new dxt: 
    time_new = (rounded_dx * Nx) / wsp

    # Check if time_new is equal or greater than the original time:
    if time_new < time:
        print('Warning: The new time is smaller than the original time.')   

    return rounded_dx, time_new


# %%

def transf_fwV(V, deltat, wsp, time_hawc2, hawc2_deltat):
    """
    Transform the free wind values into the same time steps that the original turbulence box has.
    """
    #dt = dxt / wsp
    # Find the index of the time step in the HAWC2 results that is closest to dxt:
    index = np.where(np.isclose(time_hawc2, deltat, atol=0.01))[0].item()
    div = int(deltat / hawc2_deltat)
    # This will return the free wind values into the same time steps that the original turbulence box has:
    V1 = V[index:]
    VV = V1[::div]

    return VV

# %% Function to plot subplot time series of u, v and w components:

def plot_series(time_u_random, u_compared, time, Vyy, 
                label_tbox, label_V, title, subplot_index):
    """
    Plot the comparison of the time series between the turbulence box and HAWC2 results.

    Parameters
    ----------
    time_u_random : numpy.ndarray
        Time array of the turbulence box.
    u_compared : numpy.ndarray
        Wind speed array of the turbulence box.
    time : numpy.ndarray
        Time array of the HAWC2 results.
    Vyy : numpy.ndarray
        Wind speed array of the HAWC2 results.
    label_tbox : str
        Label for the turbulence box wind speed.
    label_Vyy : str
        Label for the HAWC2 wind speed.
    title : str 
        Title of the plot.
    subplot_index : int
        Index of the subplot.
    
    Returns
    -------
        None.

    """
    # Plot the interpolated u_random time series
    plt.subplot(3, 1, subplot_index)
    plt.plot(time_u_random, u_compared, label=label_tbox)
    plt.plot(time, Vyy, label=label_V, linestyle='--')
    plt.legend(loc='upper right')
    plt.xlim([0, Time[-1]])

    if subplot_index == 3:
        plt.xlabel('Time [s]', fontsize=12, fontweight='bold')

    plt.ylabel('Wind Speed [m/s]', fontsize=12, fontweight='bold')
    plt.title(title, fontsize=14, fontweight='bold')
    plt.tight_layout()


# %% Function to interpolate the turbulence box values:

def interp_tbox_values(path_res, path_turb, fname, deltat,
                       perform_eval=False, return_df=False, **kw_turbgen):

    wsp = fNet.extract_wsp_text(fname, opt='wsp')
    # Extract information from HAWC2 results:
    hawc2_path = path_res + fname + '.hdf5'
    read_h2 = ReadHawc2(hawc2_path)
    hawc2_res = read_h2.ReadGtsdf()

    time = hawc2_res[:, 0]
    # Extract the wind speed components from Mann-turbulence box:
    turb_box = tbox.TurbBox(path_turb, fname, 'power_law', opt='hawc2', **kw_turbgen)

    # Flip the direction to match Global coordinate system in Yg direction:
    u = np.flip(turb_box.u_raw, axis=0)
    v = np.flip(turb_box.v_raw, axis=0)
    w = np.flip(turb_box.w_raw, axis=0)

    # Original grid dimensions for turbulence box. We replace the longitudinal direction based on time:
    x = np.linspace(-kw_turbgen['Ly']/2, kw_turbgen['Ly']/2, kw_turbgen['Ny'])
    y = np.linspace(deltat*wsp, deltat*wsp*kw_turbgen['Nx'], kw_turbgen['Nx'])
    z = np.linspace(0, -kw_turbgen['Lz'], kw_turbgen['Nz'])
    t = np.linspace(deltat, (deltat * kw_turbgen['Nx']), kw_turbgen['Nx'])

    # To generate values from the turbulence box to match hawc2:
    new_t = time[1:]
    new_y = np.linspace(new_t[0]*wsp, new_t[-1]*wsp, new_t.size)

    # Interpolation:
    interp_func_u = RegularGridInterpolator((t, x, z), u, method='linear')
    interp_func_v = RegularGridInterpolator((t, x, z), v, method='linear')
    interp_func_w = RegularGridInterpolator((t, x, z), w, method='linear')

    new_y_mesh, new_x_mesh, new_z_mesh = np.meshgrid(new_y, x, z, indexing='ij')
    points = np.array([new_y_mesh.flatten(), new_x_mesh.flatten(), new_z_mesh.flatten()]).T

    u_new_flat = interp_func_u(points)
    u_new = u_new_flat.reshape(new_y.size, len(x), len(z))

    v_new_flat = interp_func_v(points)
    v_new = v_new_flat.reshape(new_t.size, len(x), len(z))

    w_new_flat = interp_func_w(points)
    w_new = w_new_flat.reshape(new_t.size, len(x), len(z))

    if perform_eval:

        # New grid dimensions
        len_Nx = time.size

        # To match the dimensions lenght:
        last_value = time[-1]
        index = np.argmin(np.abs(t - last_value)) + 1

        # Range is two times to account for Y and Z grid:
        for init in range(0, kw_turbgen['Ny']*2):

            idx = 19+(3*init)

            Vx = hawc2_res[:, idx]
            Vy = hawc2_res[:, idx+1]
            Vz = hawc2_res[:, idx+2]

            if init <= (kw_turbgen['Ny']-1):
                u_compared = u_new[:, kw_turbgen['Ny']//2, init]
                v_compared = v_new[:, kw_turbgen['Ny']//2, init]
                w_compared = -1*(w_new[:, kw_turbgen['Ny']//2, init])

            elif init >= kw_turbgen['Ny']:

                u_compared = u_new[:, init-kw_turbgen['Ny'], kw_turbgen['Ny']//2]
                v_compared = v_new[:, init-kw_turbgen['Ny'], kw_turbgen['Ny']//2]
                w_compared = -1*(w_new[:, init-kw_turbgen['Ny'], kw_turbgen['Ny']//2])

            else:
                print('outside scope')

                # Evaluation of time series:
                u_close = np.allclose(v_compared, Vx[1:], atol=1e-1)
                v_close = np.allclose(u_compared, Vy[1:], atol=1e-1)
                w_close = np.allclose(w_compared, Vz[1:], atol=1e-1)

                # Cross correlation between time series:
                cross_correlation = np.correlate(Vy[1:], u_compared, "full")
                shift_u = cross_correlation.argmax() - (len(Vy[1:]) - 1)

                cross_correlation = np.correlate(Vx[1:], v_compared, "full")
                shift_v = cross_correlation.argmax() - (len(Vx[1:]) - 1)

                cross_correlation = np.correlate(Vz[1:], w_compared, "full")
                shift_w = cross_correlation.argmax() - (len(Vz[1:]) - 1)

                print(f'\nFor case: {init}, index: {idx}:')
                print(f'u component: All close {u_close}, and the signal shift is {shift_u}')
                print(f'v component: All close {v_close}, and the signal shift is {shift_v}')
                print(f'w component: All close {w_close}, and the signal shift is {shift_w}')

                with open('tbox_interpolation_results.txt', 'a') as f:
                    f.write(f'\nFor case: {init}, index: {idx}:')
                    f.write(f'\nu component: All close {u_close}, and the signal shift is {shift_u}')
                    f.write(f'\nv component: All close {v_close}, and the signal shift is {shift_v}')
                    f.write(f'\nw component: All close {w_close}, and the signal shift is {shift_w}')

    if return_df: 

        Xg, Yg, Zg = np.meshgrid(x, new_y, z, indexing='ij')
        Xg_flat = Xg.flatten()
        Yg_flat = Yg.flatten()
        Zg_flat = Zg.flatten()
        df = pd.DataFrame({'Xg': Xg_flat, 'Yg': Yg_flat, 'Zg': Zg_flat,
                           't': new_t,
                            'u': u_new_flat, 'v': v_new_flat, 'w': w_new_flat})
        return df
        
    else: 
        return u_new, v_new, w_new, new_t

