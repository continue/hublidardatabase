# -*- coding: utf-8 -*-
"""
@author: espa

This script extracts the data from multiple results files from HAWC2, to
generate the NETcdf4 file, with all the information required from the 
simulation. 

It contains the aeroelastic response for the wind turbine, with all the
hub-lidar measurements for the simulation, for 10-minute period. 

The NetCDF file is generated with the following data:
    - Aeroelastic response from HAWC2 simulation (group: wt_res).
    - Hub-lidar measurements (group: huli_data).
    - Mann turbulence box values (group: mann_tbox).

"""

import pandas as pd
import hulidb.functions_NETcdf as fNet
from hulidb.database_generation import create_lidar_config_htc
from _ltb_values import (kw_dtu10mw, kw_turbgen, lidar_arg)
from _var_names import turb_box_metadata, mann_tbox_metadata
import hulidb.functions_tbox as ftbox

# %%   

Cases = fNet.generate_Cases(**kw_turbgen)

# Compression level for NETcdf file: 
compression_level = 5

df_lidar = create_lidar_config_htc(kw_dtu10mw['z_hub'], lidar_arg)
df_lidar = df_lidar.reset_index(drop=True)
df_lidar = df_lidar.drop(['Beggining', 'Tag'], axis=1)

total_beams = len(df_lidar)

path = './dtu_10mw/res/'
t_start, t_end = 100.05, 700.05  # To have 10-min simulation data
deltat_hawc2 = 0.05  # time step output hawc2
num_beam_per_file = 2000  # Number of beams per aeroelastic simulation

part_number = fNet.calc_part_num(df_lidar, num_beam_per_file)

for case in Cases: 

    file_path = case + '.nc'
    
    # Initizialization of the NETcdf file for the inflow case:
    fNet.netCDF4_initialization(file_path, compression_level, total_beams,
                                t_start, t_end, deltat_hawc2)

    fname = fNet.generate_fname(case, part_number)
    idx_initial = kw_dtu10mw['init_idx_lidar']
    num_beams = lidar_arg['beamno_htc']

    # Update of aeroelastic response from HAWC2 results:    
    df_wt_res = fNet.read_out_channels(path, fname, t_start, t_end)
    fNet.update_wt_res(file_path, df_wt_res)

    # Update Hub-lidar data:
    fNet.update_huli_data(file_path, path, fname,
                     num_beams, total_beams, 
                     t_start, t_end, idx_initial)

    print(f'NetCDF file has been updated for {case}.')
    
    # Adding Mann turbulence box values to the NetCDF file:
    step = 10
    t_simu = 800 # Total time size of the box.
    wsp = fNet.extract_wsp_text(case, opt='wsp')
    dxt, _, deltat = ftbox.calc_dxt(step, wsp, t_simu)
    dxt = round(dxt, 2)
    
    fNet.add_mann_turbbox_to_netcdf(case, mann_tbox_metadata, 
                                    dxt, deltat, **kw_turbgen)
    
    # Testing turbulence box:        
    fNet.test_turbbox_netcdf(case, dxt, kw_turbgen)
