"""
@author: espa

Generates the htc files with the different input based on the turbulence
box parameters. It will perform loops between wind speed and seeds.

Three different sets of files are generated, and saved in different folders:
    - For turbulence boxes generation.
    - For aeroelastic simulation for hub-lidar data extraction. This file will
    be divided into multiple part, since a limit of 2000 beam outputs has been
    stablish for HuLiDB due to HAWC2 computational time constraints when 
    handling outputs above this number.
    - For generation of free wind components from turbulence box as HAWC2 reads.
    This is for testing and verification purposes (optional).

"""
import pandas as pd
from _ltb_values import kw_turbgen, lidar_arg, kw_dtu10mw
from hulidb.database_generation import (create_lidar_config_htc, 
                                     generate_htc_tb, make_htc_turb)
from wetb.wind.turbulence.mann_parameters import var2ae
import hulidb.functions_tbox as ftbox


# %% 01. Make Lidar Configurations text file for htc:
    
# Initial inputs for htc generation: 
orig_htc = './dtu_10mw/htc/DTU_10MW_orig_v2.htc'
tstop = 700.10 
tilt = 5
center_pos = -200  # Center position for turbulence box
step = 10
t_simu = 800 # Total time size of the box.

# To generate the beams required based on input parameters for configurations:
df_lidar = create_lidar_config_htc(kw_dtu10mw['z_hub'], lidar_arg)
df_lidar = df_lidar.reset_index(drop=True)

# Generates htc files for turbulence box generation:
generate_htc_tb(orig_htc, center_pos, **kw_turbgen)

# %% Generate htc files for HAWC2 simulation.

for wsp in kw_turbgen['wsps']:    
    for seed in kw_turbgen['Seeds']:
        
        print(f'\nGenerating htc file for wind speed: {wsp}, seed: {seed},'
              ' with flexible tower and tilt: {tilt} [deg] \n')
        
        rot = [0, 0, 0]   # Rotation to apply yaw in the plane.
        
        dxt, _, deltat = ftbox.calc_dxt(step, wsp, t_simu)
        print(dxt, Nx, deltat)

        # Based on the IEC Standard:
        sigma = kw_turbgen['I_ref'] * (0.75 * wsp + 5.6)
        variance = sigma**2

        sample_frq = 1/(kw_turbgen['Time']/kw_turbgen['Nx'])  

        ae = var2ae(variance, kw_turbgen['L'],
                    kw_turbgen['gamma'], wsp,
                    T=kw_turbgen['Time'],
                    sample_frq=sample_frq, plt=False)

        print(f'Calculated alpha_epsilon: {ae:.3f}')

        # For Hub-lidar Data simulation:
        idx_start = 0
        idx_end = lidar_arg['beamno_htc']
        total_simulations = (len(df_lidar) // idx_end) + 1

        for i in range(total_simulations):
            if idx_end > len(df_lidar):
                idx_end = len(df_lidar)

            fname = (kw_turbgen['turb_model'] + '_wsp_' + str(wsp) + '_seed_' +
                     str(seed) + f'_ae_{ae:.3f}')
            file_name = fname + f'_part_{i+1}'
            turb_name = fname

            make_htc_turb(
                orig_htc, file_name, df_lidar, idx_start, idx_end, wsp, 
                rot, tstop, turb_name, ae, center_pos, seed, 
                shear_format='none', turb=True, turb_none_exist=False, dxt=dxt,
                shear_turb=True, tstart=100, tower_flex=True,
                tilt=tilt, yaw=0, roll=0, **kw_turbgen)          
            
            idx_start = idx_end
            idx_end = idx_start + lidar_arg['beamno_htc']
            
print(f'Generation of htc files for dataset simulation are'
      ' completed, and can be found in ./dtu_10mw/htc/lidar_simulation/')

# %% Generates htc file to extract turbulence box flow as HAWC2 read it: 

free_wind = False

if free_wind:

    step = 10
    idx_start, idx_end = 0, 0
    orig_htc = './dtu_10mw/htc/DTU_10MW_orig_free_wind.htc'
    center_pos = kw_turbgen['box_center']
    rot = [0, 0, 0]   # Rotation to apply yaw in the plane. 
    df_lidar = pd.DataFrame()  # Empty Dataframe, for hub-lidar beams

    for wsp in kw_turbgen['wsps']:    
        for seed in kw_turbgen['Seeds']:
                            
            # Based on the IEC Standard:
            sigma = 0.14 * (0.75 * wsp + 5.6)
            variance = sigma**2
            
            dxt, _, deltat = ftbox.calc_dxt(step, wsp, t_simu)
            print(dxt, deltat)
            
            sample_frq = 1/(kw_turbgen['Time']/kw_turbgen['Nx'])  
            
            ae = var2ae(variance, kw_turbgen['L'], kw_turbgen['gamma'], wsp,
                        T=kw_turbgen['Time'], sample_frq=sample_frq, plt=False)
            
            print(f'Calculated alpha_epsilon: {ae:.3f}')
            
            # For Turbulence Box Generation:
            file_name = (kw_turbgen['turb_model'] + '_wsp_' + str(wsp) +
                        '_seed_' + str(seed) + f'_ae_{ae:.3f}')
                            
            # Turbulence box with shear profile added from Python:
            turb_name = file_name
            fname = file_name
            
            make_htc_turb(
                orig_htc, fname, df_lidar, idx_start, idx_end, wsp,
                rot, tstop, turb_name, ae, center_pos, seed,
                shear_format='none', turb=True, turb_none_exist=False, dxt=dxt,
                shear_turb=True, tstart=100, tower_flex=True,
                tilt=0, yaw=0, roll=0, free_wind=True, path_htc='free_wind/',
                **kw_turbgen)
        
